# Augmentation

TODO

This script can be used to augment data sets. Convert one or more ROS Bags or extracted data 
into augmented data. This script is used to convert ROS Message Data saved in the ROS Bagfile 
format into an easier to consume file format that eases further processing, augmentation 
and training.

<table>
  <tr>
    <td valign="top">
    
## Content
- [Help](#help)
- [Config File](#config-file) 
- [Output](#output)
    </td>
  </tr>
</table>


## Usage

Just run the following terminal command to execute the script:

```bash
python augment.py -c example_configs/augment.yaml
```

#### Help

To print the help:

```bash
python analyze.py -h
# or
python analyze.py --help
 ```

The help info should look like this:

```bash
usage: augment.py [-h] [-c CONFIGFILE]

Convert one or more ROS Bags or extracted data into augmented data.
This script is used to convert ROS Message Data saved in the ROS Bagfile format into 
an easier to consume file format that eases furtherprocessing, augmentation and training.

optional arguments:
  -h, --help            show this help message and exit

config:
  -c CONFIGFILE, --configfile CONFIGFILE
                        A path to a configuration file in yaml syntax
``` 

#### Config File

You can specify a yaml configuration file. An example config file can be found here 
[augment.yaml](./example_configs/augment.yaml). To read more about the config file click here: 
[cli-augment config file](cli-augment_config.md).

```bash
python augment.py -c example_configs/augment.yaml
# or
python augment.py --configfile example_configs/augment.yaml
```

#### Output

After the augmentation the cli outputs the results in the given 
`destination: "/path/to/dir"`. The result contains the following files:

```bash
/path/to/output/data/
|
\-- {source_name}_augmented1/
    +-- {source_name}_augmented1.yaml
    \-- images

```