from __future__ import print_function

import os

from torchvision import datasets, transforms

from neuroracer_ai_sl.utils.data_reader import AbstractDataReader

# default path for external data provided by PyTorch
DATA_PATH = os.path.expanduser('~')
if not os.access(DATA_PATH, os.W_OK):
    _nr_base_dir = '/tmp'
DATA_PATH = os.path.join(DATA_PATH, '.neurorace/external_data')


def mnist(batch_size):
    """
    Returns MNIST train and eval keras.utils.Sequence

    """

    class MNISTDataLoader(AbstractDataReader):
        def __init__(self, data, _batch_size, n_total_data):
            super(MNISTDataLoader, self).__init__()
            self.data_list = data
            self.batch_size = _batch_size
            self._prepare(n_total_data=n_total_data)

        def __len__(self):
            return self.n_total_data

        def __getitem__(self, idx):
            # This will process data on thy fly. For MNIST it is directly processed by PyTorch after loading
            # data for the whole set. But this simple example demonstrates the process as used later
            # by the neuroracer data handler (here the data is just returned, where as the neuroracer framework
            # uses different pipelines for each required task. At the end of the pipeline chain, the processed batches
            # are returned as x and y like shown in this example).

            x, y = self.data_list[idx]

            return x, y

    # get training data
    train_data = datasets.MNIST(DATA_PATH, train=True, download=True,
                                transform=transforms.Compose([
                                    transforms.ToTensor(),
                                    transforms.Normalize((0.1307,), (0.3081,))
                                ]), )

    # get evaluation data
    eval_data = datasets.MNIST(DATA_PATH, train=False,
                               transform=transforms.Compose([
                                   transforms.ToTensor(),
                                   transforms.Normalize((0.1307,), (0.3081,))
                               ]))

    # create keras sequence
    train_sequence = MNISTDataLoader(data=train_data, _batch_size=batch_size, n_total_data=len(train_data))
    eval_sequence = MNISTDataLoader(data=eval_data, _batch_size=batch_size, n_total_data=len(eval_data))

    return train_sequence, eval_sequence
