from __future__ import print_function

import numpy as np
from keras import datasets
from keras.utils import np_utils, Sequence
from neuroracer_ai_sl.utils.data_reader import AbstractDataReader

"""
Note:

All data functions require at least given batch_size as parameter.

For further information on how to implement keras.utils.Sequences, see
https://stanford.edu/~shervine/blog/keras-how-to-generate-data-on-the-fly
"""


def chunk_it(data, batch_size):
    """
    Create batch-sized chunks

    :param data:
    :param batch_size:
    :return:
    """
    return [data[i * batch_size:(i + 1) * batch_size] for i in range((len(data) + batch_size - 1) // batch_size)]


def cifar10(batch_size, nb_classes=10):
    """
    Returns CIFAR10 train and eval keras.utils.Sequence

    """

    class CIFAR10Sequence(Sequence, AbstractDataReader):
        def __init__(self, data, _batch_size, n_total_data):
            super(CIFAR10Sequence, self).__init__()
            self.data_list = data
            self.batch_size = _batch_size
            self._prepare(n_total_data=n_total_data)

        def __len__(self):
            return self.batches_per_epoch

        def __getitem__(self, idx):
            # This will process data on thy fly. For CIFAR it could be directly processed after loading
            # data for the whole set. But this simple example demonstrates the process as used later
            # by the neuroracer data handler (here the data is directly processed, where as the neuroracer framework
            # uses different pipelines for each required task. At the end of the pipeline chain, the processed batches
            # are returned as x and y like shown in this example).

            # get batch
            x, y = self.data_list[0][idx], self.data_list[1][idx]

            # process data

            # normalize images
            x = x.astype('float32') / 255
            # convert class vectors to binary class matrices
            y = np_utils.to_categorical(y, nb_classes)

            return np.array(x), np.array(y)

    # load custom data
    (x_train, y_train), (x_test, y_test) = datasets.cifar10.load_data()

    # prepare data for network
    x_train = x_train.reshape(x_train.shape[0], 32, 32, 3)
    x_test = x_test.reshape(x_test.shape[0], 32, 32, 3)

    # combine to (batch-sized) chunks of tuples
    train_data = (chunk_it(x_train, batch_size), chunk_it(y_train, batch_size))
    eval_data = (chunk_it(x_test, batch_size), chunk_it(y_test, batch_size))

    # create keras sequence
    train_sequence = CIFAR10Sequence(data=train_data, _batch_size=batch_size, n_total_data=x_train.shape[0])
    eval_sequence = CIFAR10Sequence(data=eval_data, _batch_size=batch_size, n_total_data=x_test.shape[0])

    return train_sequence, eval_sequence


def cifar100(batch_size, nb_classes=100):
    """
    Returns CIFAR100 train and eval keras.utils.Sequence

    """

    class CIFAR100Sequence(Sequence, AbstractDataReader):
        def __init__(self, data, _batch_size, n_total_data):
            super(CIFAR100Sequence, self).__init__()
            self.data_list = data
            self.batch_size = _batch_size
            self._prepare(n_total_data=n_total_data)

        def __len__(self):
            return self.batches_per_epoch

        def __getitem__(self, idx):
            # This will process data on thy fly. For CIFAR it could be directly processed after loading
            # data for the whole set. But this simple example demonstrates the process as used later
            # by the neuroracer data handler (here the data is directly processed, where as the neuroracer framework
            # uses different pipelines for each required task. At the end of the pipeline chain, the processed batches
            # are returned as x and y like shown in this example).

            # get batch
            x, y = self.data_list[0][idx], self.data_list[1][idx]

            # normalize images
            x = x.astype('float32') / 255
            # convert class vectors to binary class matrices
            y = np_utils.to_categorical(y, nb_classes)

            return np.array(x), np.array(y)

    # load custom data
    (x_train, y_train), (x_test, y_test) = datasets.cifar100.load_data()

    # prepare data for network
    x_train = x_train.reshape(x_train.shape[0], 32, 32, 3)
    x_test = x_test.reshape(x_test.shape[0], 32, 32, 3)

    # combine to (batch-sized) chunks of tuples
    train_data = (chunk_it(x_train, batch_size), chunk_it(y_train, batch_size))
    eval_data = (chunk_it(x_test, batch_size), chunk_it(y_test, batch_size))

    # create keras sequence
    train_sequence = CIFAR100Sequence(data=train_data, _batch_size=batch_size, n_total_data=x_train.shape[0])
    eval_sequence = CIFAR100Sequence(data=eval_data, _batch_size=batch_size, n_total_data=x_test.shape[0])

    return train_sequence, eval_sequence


def mnist(batch_size, nb_classes=10):
    """
    Returns MNIST train and eval keras.utils.Sequence

    """

    class MNISTSequence(Sequence, AbstractDataReader):
        def __init__(self, data, _batch_size, n_total_data):
            super(MNISTSequence, self).__init__()
            self.data_list = data
            self.batch_size = _batch_size
            self._prepare(n_total_data=n_total_data)

        def __len__(self):
            return self.batches_per_epoch

        def __getitem__(self, idx):
            # This will process data on thy fly. For MNIST it could be directly processed after loading
            # data for the whole set. But this simple example demonstrates the process as used later
            # by the neuroracer data handler (here the data is directly processed, where as the neuroracer framework
            # uses different pipelines for each required task. At the end of the pipeline chain, the processed batches
            # are returned as x and y like shown in this example).

            # get batch
            x, y = self.data_list[0][idx], self.data_list[1][idx]

            # normalize images
            x = x.astype('float32') / 255
            # convert class vectors to binary class matrices
            y = np_utils.to_categorical(y, nb_classes)

            return np.array(x), np.array(y)

    # load custom data
    (x_train, y_train), (x_test, y_test) = datasets.mnist.load_data()

    # prepare data for network
    x_train = x_train.reshape(x_train.shape[0], 28, 28, 1)
    x_test = x_test.reshape(x_test.shape[0], 28, 28, 1)

    # combine to (batch-sized) chunks of tuples
    train_data = (chunk_it(x_train, batch_size), chunk_it(y_train, batch_size))
    eval_data = (chunk_it(x_test, batch_size), chunk_it(y_test, batch_size))

    # create keras sequence
    train_sequence = MNISTSequence(data=train_data, _batch_size=batch_size, n_total_data=x_train.shape[0])
    eval_sequence = MNISTSequence(data=eval_data, _batch_size=batch_size, n_total_data=x_test.shape[0])

    return train_sequence, eval_sequence
