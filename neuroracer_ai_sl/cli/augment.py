#!/usr/bin/env python

"""
This script augments the data specified by the sources.
"""

from __future__ import division, print_function

import functools

from neuroracer_ai.utils.arg_handler import get_argparser
from neuroracer_ai.utils.config_handler import NB_CPU
from neuroracer_ai.utils.config_handler import check_config, read_config_file
from neuroracer_ai.utils.snapshot import ImageSnapshot

from neuroracer_ai_sl.utils.augment_functions import augment_rotation, augment_brightness
from neuroracer_ai_sl.utils.bag_reader import RosbagReader
from neuroracer_ai_sl.utils.data_handler import get_container_name, write_snapshot_dict
from neuroracer_ai_sl.utils.data_reader import ExtractedDataReader


# For examples for augmentation see here: https://github.com/aleju/imgaug


def main():
    """
    Augments specified sources.
    """

    args = _get_arguments()

    config = {"bag_sources": None,
              "extracted_sources": None,
              "frame_skip_bags": 0,
              "frame_skip_extracted": 0,
              "verbose": 0
              }

    if args.configfile:
        config_from_file = read_config_file(args.configfile)
        config.update(config_from_file)

    # override config file configurations with action line arguments
    for arg_name, arg_value in vars(args).items():
        if arg_value is not None:
            config[arg_name] = arg_value

    verbose = config["verbose"]
    and_required_keys = ["destination", "compositions"]
    or_required_keys = [("bag_sources", "extracted_sources")]

    check_config(config, and_required_keys=and_required_keys, or_required_keys=or_required_keys)

    if config["bag_sources"]:
        snapshot_generator = RosbagReader(bag_sources=config["bag_sources"],
                                          rosbag_batch_size=config["rosbag_batch_size"],
                                          camera_topics=config["camera_topics"],
                                          action_topics=config["action_topics"],
                                          frame_skip=config["frame_skip_bags"],
                                          nb_cpu=NB_CPU,
                                          shuffle=False,
                                          return_dict=True,
                                          loop_indefinitely=False)

    else:
        snapshot_generator = ExtractedDataReader(main_dir=config["extracted_sources"],
                                                 batch_size=config["snapshot_batch_size"],
                                                 nb_cpu=NB_CPU,
                                                 shuffle=False,
                                                 return_dict=True,
                                                 loop_indefinitely=False)

    compositions = config["compositions"]

    for snapshot_dict in snapshot_generator:
        augmented_dict_gen = (
            _augment_snapshots_with_compositions(item, compositions=compositions)
            for item in snapshot_dict.items())

        for augmented_snapshot_dict in augmented_dict_gen:
            write_snapshot_dict(augmented_snapshot_dict,
                                destination=config["destination"],
                                verbose=verbose,
                                nb_cpu=1)


def _augment_snapshots_with_compositions(item, compositions):
    """
    Creates a new snapshot list by augmenting the input images with the
    provided compositions.

    :param item: A tuple (source_name, snapshot_list)
    :type item: tuple
    :param compositions: A list of compositions to apply to the snapshot_list
    :type compositions: List[Dict]
    :return: A dictionary with the created container_names as keys and the
             corresponding snapshot_lists as values
    :rtype: dict[str, List[neuroracer_ai.utils.ImageSnapshot]]
    """

    source_name, snapshot_list = item
    augmented_snapshot_dict = {}

    for composition in compositions:
        container_name = get_container_name(composition["name"].format(source_name=source_name))
        augmented_snapshot_list = _augment_snapshots(snapshot_list, composition)

        if container_name not in augmented_snapshot_dict:
            augmented_snapshot_dict[container_name] = []

        augmented_snapshot_dict[container_name].extend(augmented_snapshot_list)

    return augmented_snapshot_dict


def _build_augmentation_pipeline(composition):
    """
    Constructs a single function that applies all augmentations defined by the
    composition parameter to an ImageSnapshot object.

    :param composition: The composition defining multiple augmentations
    :type composition: dict
    :return: A function pipeline(snapshot) -> snapshot.
             The return value of this pipeline is an augmented snapshot.
    :rtype: Callable[[snapshot], snapshot]
    """

    # build pipeline
    def make_pipeline(snapshot):
        camera_msgs_by_topic = snapshot.get_uncompressed_camera_msgs()
        action_msgs = snapshot.action_msgs

        # for every augmentation function, go through all camera topics
        # and apply augmentations
        for aug_function in aug_functions:
            for topic, cam_msg in camera_msgs_by_topic.items():
                augmented_cam_msg, augmented_action_msgs = aug_function(image=cam_msg,
                                                                        actions=action_msgs)
                camera_msgs_by_topic[topic] = augmented_cam_msg
                action_msgs = augmented_action_msgs

        return ImageSnapshot.copy(snapshot,
                                  camera_msgs=camera_msgs_by_topic,
                                  action_msgs=action_msgs)

    aug_functions = []

    # rotation
    if "rotation" in composition:
        rotation = composition["rotation"]
        if "intensity" not in rotation:
            raise KeyError("You have to specify 'intensity' of rotation in composition '{}'"
                           .format(composition["name"]))

        rotation_func = functools.partial(augment_rotation, intensity=rotation["intensity"])
        aug_functions.append(rotation_func)

    # brightness
    if "brightness" in composition:
        brightness = composition["brightness"]
        if "intensity" not in brightness:
            raise KeyError("You have to specify 'intensity' of brightness in composition '{}'"
                           .format(composition["name"]))

        brightness_func = functools.partial(augment_brightness,
                                            intensity=brightness["intensity"])

        aug_functions.append(brightness_func)

    return make_pipeline


def _augment_snapshots(snapshot_list, composition):
    """
    Augments the snapshot_list with the given composition.

    :param composition: The composition which defines the way of augmentation
    :type compositio   n: dict
    :param snapshot_list: The list of snapshots to augment
    :type snapshot_list: List[snapshot]

    :return: A augmented list of snapshots
    :rtype: list[neuroracer_ai.utils.ImageSnapshot]
    """

    pipeline = _build_augmentation_pipeline(composition)
    augmented_snapshot_list = []

    if "count" not in composition:
        raise KeyError("You have to specify 'count' for each composition")
    count = composition["count"]

    for snapshot in snapshot_list:
        for _ in range(count):
            augmented_snapshot = pipeline(snapshot)
            augmented_snapshot_list.append(augmented_snapshot)

    return augmented_snapshot_list


def _get_arguments():
    """
    Creates the ArgumentParser and returns the parsed arguments.

    :returns: A dictionary with the parsed action line arguments.
    """

    # get common args
    parser = get_argparser(description='Convert one or more ROS Bags into extracted data structure.\n'
                                       'This script is used to convert ROS Message Data saved in the ROS Bag'
                                       'file format into an easier to consume file format that eases further'
                                       'processing, augmentation and training.')
    return parser.parse_args()


if __name__ == '__main__':
    main()
