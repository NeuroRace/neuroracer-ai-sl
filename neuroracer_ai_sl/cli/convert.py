#!/usr/bin/env python

from neuroracer_ai.utils import config_handler
from neuroracer_ai.utils.arg_handler import get_argparser

from neuroracer_ai_sl.utils.bag_reader import RosbagReader
from neuroracer_ai_sl.utils.data_handler import get_container_name, write_snapshot_dict


def main():
    """
    Converts specified sources into faster readable data.
    """

    args = _get_arguments()

    config = {'sources': None,
              'destination': None,
              'topics': {
                  'cameras': None,
                  'actions': None},
              'rosbag_batch_size': 5,
              'verbose': 0,
              'delta_t': 5e7,
              'frame_skip': 0}

    if args.configfile:
        config = config_handler.read_config_file(args.configfile)

    # override config file configurations with action line arguments
    # for arg_name, arg_value in vars(args).items():
    #     if arg_value is not None:
    #         config[arg_name] = arg_value

    config_handler.check_config(config, and_required_keys=["sources",
                                                           "destination",
                                                           "topics",
                                                           "rosbag_batch_size",
                                                           "frame_skip"])

    rosbag_dict_generator = RosbagReader(
        bag_sources=config["sources"],
        camera_topics=config["topics"]["cameras"],
        action_topics=config["topics"]["actions"],
        frame_skip=config["frame_skip"],
        delta_t=config["delta_t"],
        rosbag_batch_size=config["rosbag_batch_size"],
        nb_cpu=config_handler.NB_CPU,
        return_dict=True,
        loop_indefinitely=False)

    # write rosbags into destination
    for rosbag_dict in rosbag_dict_generator:
        snapshot_dict = {}
        for key, snapshot_list in rosbag_dict.items():
            container_name = get_container_name(key)
            snapshot_dict[container_name] = snapshot_list

        write_snapshot_dict(snapshot_dict=snapshot_dict,
                            destination=config["destination"],
                            verbose=config["verbose"])
        del snapshot_dict


def _get_arguments():
    """
    Creates the ArgumentParser and returns the parsed arguments.

    :returns: A dictionary with the parsed command line arguments.
    """

    # get common args
    parser = get_argparser(description='Convert ROS-Bags into faster readable data.')
    return parser.parse_args()


if __name__ == '__main__':
    main()
