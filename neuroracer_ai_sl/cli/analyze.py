from __future__ import division, print_function

import math
import os
import re
import sys

import cv2
import numpy as np
from neuroracer_ai.suites import DualViewSnapshotProcessorSuite
from neuroracer_ai.suites import SingleViewSnapshotProcessorSuite
from neuroracer_ai.utils import config_handler
from neuroracer_ai.utils.arg_handler import get_argparser

from neuroracer_ai_sl import AI
from neuroracer_ai_sl.utils.data_reader import ExtractedDataReader

TRUE_STEER_COLOR = (0, 255, 0)
PRED_STEER_COLOR = (255, 255, 0)


def get_config_w_defaults():
    args = _get_arguments()

    config = {'dual_view': 1,
              'bin_size': 0.2,
              'batch_size': 128,
              'save_mean_and_std': 1,
              'compute_metrics': 1,
              'draw_eval_arrows': 0,
              'predict': 0
              }

    if args.configfile:
        config_from_file = config_handler.read_config_file(args.configfile)
        for key, val in config_from_file.items():
            config[key] = val

    return config


def remap_numbers(value, old_min, old_max, new_min, new_max):
    '''
    Remaps a number from one range into another range
    :param value: number to be remapped
    :param old_min: old ranges minimum value
    :param old_max: new ranges maximum value
    :param new_min: old ranges minimum value
    :param new_max: new ranges maximum value
    :return: remapped value
    '''
    old_span = old_max - old_min
    new_span = new_max - new_min
    return (value - old_min) * new_span / old_span + new_min


def draw_arrow_on_img(image, angle, color):
    '''
    Draws an arrow with the specified angle on an image. Arrows are drawn from
    the bottom center of the image.
    :param image: image that arrows are to be drawn on to
    :param angle: angle of the arrow, relative to the images lower center
    :param color: color of the arrow
    :return: image with arrow drawn on top
    '''
    image_width = image.shape[1]
    image_height = image.shape[0]
    image_hor_center = int(image_width / 2)
    arrow_origin = (image_hor_center, image_height)
    arrow_length = image_height / 2

    steer_angle = remap_numbers(angle, -1, 1, -90, 90)
    steer_angle_rad = math.radians(steer_angle)

    arrow_end = (
        image_hor_center + int(arrow_length * math.sin(steer_angle_rad)),
        image_height - int(arrow_length * math.cos(steer_angle_rad)))

    if color == TRUE_STEER_COLOR:
        cv2.putText(image, "TRUE VALUE", (0, 20), cv2.FONT_HERSHEY_PLAIN, 1, color)
    elif color == PRED_STEER_COLOR:
        cv2.putText(image, "PREDICTION", (0, 40), cv2.FONT_HERSHEY_PLAIN, 1, color)

    return cv2.arrowedLine(image, arrow_origin, arrow_end, color, 4)


def compute_mean(x):
    '''
    computes a featurewise (per colorchannel) mean over an image array
    :param x: image array
    :return: featurewise mean
    '''
    x = np.asarray(x)
    mean = np.mean(x, axis=(0, 1, 2))
    return mean.reshape((1, 1, 3))


def compute_feature_wise_std_dev(x):
    '''
    computes a featurewise (per colorchannel) standard deviation over an array
    :param x: image array
    :return: featurewise standard deviation
    '''
    x = np.asarray(x)
    std = np.std(x, axis=(0, 1, 2))
    return std.reshape((1, 1, 3))


def clear_previous_text(prev_text):
    '''
    clears text written to stdout so it can be overwritten with new text
    :param prev_text: the text to be cleared,
                      needed to compute how much to overwrite
    :return: None
    '''

    def move_cursor_up(n_lines):
        for _ in range(n_lines):
            sys.stdout.write("\x1b[A")

    n_lines = prev_text.count('\n')
    move_cursor_up(n_lines)
    sys.stdout.write(re.sub(r"[^\s]", " ", prev_text))  # write spaces over old text
    move_cursor_up(n_lines)


def main():
    config = get_config_w_defaults()
    ai = AI.load(name=config['model_name'], model_dir=config['model_dir'])

    if config['dual_view']:
        processing_suite = DualViewSnapshotProcessorSuite(camera_topics=config["topics"]["cameras"],
                                                          action_topics=config["topics"]["actions"])
    else:
        processing_suite = SingleViewSnapshotProcessorSuite(camera_topic=config["topics"]["cameras"],
                                                            action_topics=config["topics"]["actions"])
    snapshot_generator = ExtractedDataReader(
        main_dir=config['sources'],
        batch_size=config['batch_size'],
        nb_cpu=config_handler.NB_CPU,
        return_dict=True,
        loop_indefinitely=False)

    n = 0.
    total_img_counter = 0
    straight_steer_val = 0.0
    bin_size = config['bin_size']
    bins = np.arange(-1, 1 + bin_size, bin_size)

    images = []
    steer_vals_true = []
    steer_vals_pred = []
    mae_only_straight = []
    mse_only_straight = []
    prev_text = ''

    for snapshot_dict in snapshot_generator:
        for container_name, snapshot_batch in snapshot_dict.items():

            images_as_array, steer_vals = processing_suite.process(snapshot_batch=snapshot_batch)

            if config['predict']:
                predicted_steers = [pred[0] for pred in ai.predict(images_as_array)]

            if config['compute_metrics']:
                steer_vals_true.extend(steer_vals)
                mae_only_straight.extend([abs(ts - straight_steer_val) for ts in steer_vals])
                mse_only_straight.extend([abs(ts - straight_steer_val) ** 2 for ts in steer_vals])

                if n < 1e4:  # use max 10.000 images to compute mean and std
                    _, _, height, width, color_channels = images_as_array.shape
                    images.extend(images_as_array.reshape((-1, height, width, color_channels)))
                    mean = compute_mean(images)
                    std_dev = compute_feature_wise_std_dev(images)

                if config['predict']:
                    steer_vals_pred.extend(predicted_steers)

                n += len(snapshot_batch)

                mae = np.mean([abs(ts - ps) for ts, ps in zip(steer_vals_true, steer_vals_pred)])
                mse = np.mean([abs(ts - ps) ** 2 for ts, ps in zip(steer_vals_true, steer_vals_pred)])

                msg = '#' * 100
                msg += '\n{:<30} {:<10.4f} \t shape:{:<10}'.format('mean of images:', np.average(mean), mean.shape)
                msg += '\n{:<30} {:<10.4f} \t shape:{:<10}'.format('std dev of images:', np.average(std_dev),
                                                                   std_dev.shape)
                msg += '\n'
                msg += '\n{:<30} {:<10.4f}'.format('MAE only driving straight:', np.mean(mae_only_straight))
                msg += '\n{:<30} {:<10.4f}'.format('MSE only driving straight:', np.mean(mse_only_straight))

                if config['predict']:
                    msg += '\n{:<30} {:<10.4f}'.format('Mean Absolute Error:', mae)
                    msg += '\n{:<30} {:<10.4f}'.format('Mean Squared Error:', mse)
                    msg += '\n'
                    msg += '\nHistogram of predicted steering values'
                    for percentage, bin_label in zip(*np.histogram(steer_vals_pred, bins=bins)):
                        msg += '\n{:.2f}: {}'.format(bin_label, percentage)
                    msg += '\n'

                msg += '\nHistogram of true steering values'
                for percentage, bin_label in zip(*np.histogram(steer_vals_true, bins=bins)):
                    msg += '\n{:.2f}: {}'.format(bin_label, percentage)
                msg += '\n'
                msg += '#' * 100
                msg += '\n'

                clear_previous_text(prev_text)
                sys.stdout.write(msg)
                prev_text = msg

            if config['draw_eval_arrows']:
                output_dir = os.path.join(config['output_dir'], container_name)
                if not os.path.exists(output_dir):
                    os.makedirs(output_dir)

                for idx, (image_pair, steer_true) in enumerate(zip(images_as_array, steer_vals)):
                    new_img = draw_arrow_on_img(image_pair[0], steer_true, TRUE_STEER_COLOR)

                    if config['predict']:
                        pred_steer = predicted_steers[idx]
                        new_img = draw_arrow_on_img(new_img, pred_steer, PRED_STEER_COLOR)

                    new_img_path = os.path.join(output_dir,
                                                'img_with_steer_arrows_{}.jpg'.format(total_img_counter))
                    cv2.imwrite(new_img_path, new_img)

                    total_img_counter += 1

        if config['save_mean_and_std'] and config['compute_metrics']:
            mean = compute_mean(np.asarray(images))
            std_dev = compute_feature_wise_std_dev(np.asarray(images))
            np.save(os.path.join(config['output_dir'], 'mean.npy'), mean)
            np.save(os.path.join(config['output_dir'], 'std_dev.npy'), std_dev)


def _get_arguments():
    '''
    Creates the ArgumentParser and returns the parsed arguments.
    :returns: A dictionary with the parsed command line arguments.
    '''

    # get common args
    parser = get_argparser(description='Compute Metrics over Dataset\n'
                                       'This script can be used to analyze a dataset and compute the featurewise'
                                       'mean and standard deviation, as well as a distribution of the steering'
                                       'angles contained within the data. Optionally, predictions made by a '
                                       'trained model can be evaluated by augmenting the images in the dataset'
                                       'with the true and predicted steering angles')
    return parser.parse_args()


if __name__ == '__main__':
    main()
