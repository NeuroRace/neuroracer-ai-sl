# Converter

## Config File

The config file should look like this:

```yaml
sources: ["/path/to/dir", "/path/to/dir2",                   # list of dirs to bag files or
          "/path/to/bag/file.bag", "/path/to/bag/file2.bag"] # single bag files
          
destination: "/path/to/dir" # path to the destination dir

verbose: 1           # 0 or 1 to set the verbose mode

frame_skip: 0        # Number of frames to skip after reading a frame.
                     # frame_skip=0: take all frames
                     # frame_skip=1: take every second frame
                     
rosbag_batch_size: 5 # How many rosbags should be processed in every call

delta_t: 5e7         # The maximum time delta for two messages to be
                     # considered correlated, in nanosecs

topics:
  cameras: ["/zed/left/image_raw_color/compressed"] # list of camera topics
  actions: ["/nr/engine/input/actions"]             # list of action topics
```
