from __future__ import division

from itertools import repeat
from math import floor

from neuroracer_common.version_solver import get_ABC_container
from torch.nn import Module

container_abcs = get_ABC_container()


def _ntuple(n):
    def parse(x):
        if isinstance(x, container_abcs.Iterable):
            return x
        return tuple(repeat(x, n))

    return parse


_single = _ntuple(1)
_pair = _ntuple(2)
_triple = _ntuple(3)
_quadruple = _ntuple(4)


class AbstractNet(Module):
    """
    Abstract Module interface. We keep the abstract naming even if the class does not inherit from ABC
    for consistent naming in our API.
    """

    def __init__(self, input_shape, learning_rate=1e-03):
        """
        Required to be called by all subclasses.

        :param input_shape:
        :param learning_rate:
        """
        super(AbstractNet, self).__init__()

        self.input_shape = input_shape
        self.learning_rate = learning_rate

    def forward(self, x):
        """
        Define the input-forwarding through the layers of the network.

        Required to be overridden by all subclasses.

        :param x: defined input
        :return: defined prediction output of network
        """
        raise NotImplementedError

    def get_optimizer(self):
        """
        Define optimizer for network.

        Required to be overridden by all subclasses.

        :return:torch.optim.SomeOptimizer
        """
        raise NotImplementedError

    def get_loss_function(self):
        """
        Define loss function for network

        Required to be overridden by all subclasses.

        :return: torch.nn.SomeLoss
        """
        raise NotImplementedError

    @staticmethod
    def _weights_init(m):
        """
        Initialize layers (optional).

        Further information
        https://pytorch.org/docs/master/nn.html?highlight=init#torch-nn-init

        Example
        https://discuss.pytorch.org/t/how-are-layer-weights-and-biases-initialized-by-default/13073/25

        :param m:
        """
        pass

    """
    Static helper methods.
    
    Explanations:
    https://github.com/vdumoulin/conv_arithmetic/blob/master/README.md
    """

    @staticmethod
    def maxpool2d_output_shape(h_w, kernel_size, stride=None, padding=0, dilation=1):
        """
        Source
        https://pytorch.org/docs/master/nn.html?highlight=conv2d#torch.nn.MaxPool2d

        Formula (derived from tensorflow)
        https://stackoverflow.com/questions/37674306/what-is-the-difference-between-same-and-valid-padding-in-tf-nn-max-pool-of-t/37674568#37674568

        Utility function for computing output of a 2-dimensional max pooling.
        Takes a tuple of (h,w) and returns a tuple of (h,w).

        :param h_w: TODO
        :param kernel_size: TODO
        :param stride: TODO
        :param padding: TODO
        :param dilation: TODO not required?
        :return: TODO
        """

        h_w = _pair(h_w)
        kernel_size = _pair(kernel_size)
        stride = _pair(stride) if stride else kernel_size
        padding = _pair(padding)

        if padding == (0, 0):
            h = floor(h_w[0] / stride[0])
            w = floor(h_w[1] / stride[1])
        else:
            h = floor((h_w[0] - kernel_size[0] + 1) / stride[0])
            w = floor((h_w[1] - kernel_size[1] + 1) / stride[1])

        return h, w

    @staticmethod
    def conv2d_output_shape(h_w, kernel_size, stride=1, padding=0, dilation=1):
        """
        Source
        https://pytorch.org/docs/master/nn.html?highlight=conv2d#torch.nn.Conv2d

        Formula
        https://discuss.pytorch.org/uploads/default/original/2X/f/fab7651734d4000309a49736e88fc8e8b0bc2221.png

        Derived from
        https://discuss.pytorch.org/t/utility-function-for-calculating-the-shape-of-a-conv-output/11173/6

        Utility function for computing output of convolutions (2-dimensional).
        Takes a tuple of (h,w) and returns a tuple of (h,w).

        :param h_w: TODO
        :param kernel_size: TODO
        :param stride: TODO
        :param padding: TODO
        :param dilation: TODO
        :return: TODO
        """

        h_w = _pair(h_w)
        kernel_size = _pair(kernel_size)
        stride = _pair(stride)
        padding = _pair(padding)

        h = floor((h_w[0] + (2 * padding[0]) - (dilation * (kernel_size[0] - 1)) - 1) / stride[0] + 1)
        w = floor((h_w[1] + (2 * padding[1]) - (dilation * (kernel_size[1] - 1)) - 1) / stride[1] + 1)

        return h, w

    @staticmethod
    def convtransp2d_output_shape(h_w, kernel_size, stride=1, padding=0, dilation=1):
        """
        Derived from
        https://discuss.pytorch.org/t/utility-function-for-calculating-the-shape-of-a-conv-output/11173/6

        Utility function for computing output of transposed convolutions (2-dimensional).
        Takes a tuple of (h,w) and returns a tuple of (h,w).

        :param h_w: TODO
        :param kernel_size: TODO
        :param stride: TODO
        :param padding: TODO
        :param dilation: TODO not required?
        :return: TODO
        """

        h_w = _pair(h_w)
        kernel_size = _pair(kernel_size)
        stride = _pair(stride)
        padding = _pair(padding)

        h = floor((h_w[0] - 1) * stride[0] - 2 * padding[0] + kernel_size[0] + padding[0])
        w = floor((h_w[1] - 1) * stride[1] - 2 * padding[1] + kernel_size[1] + padding[1])

        return h, w
