"""
Everything which belongs to the keras backend.
"""
from __future__ import division

import os
import warnings

import numpy as np
from neuroracer_ai.models.abstract import Trainable
from neuroracer_ai.models.keras_base_backend import KerasBaseModel
from neuroracer_ai.utils.config_handler import NB_CPU
from neuroracer_ai_sl.utils.history_logger import HistoryLogger

# TODO workaround for issue #3
#  https://gitlab.com/NeuroRace/neuroracer-ai-sl/issues/3
os.environ["HDF5_USE_FILE_LOCKING"] = "FALSE"

# hide keras future warnings as we can't change them and they bleed into our output
with warnings.catch_warnings():
    warnings.simplefilter(action="ignore", category=FutureWarning)

    import keras as k
    from keras.utils import Sequence


class KerasSequence(Sequence):

    def __init__(self, data_generator, data_processor, processor_suite):
        """
        :param data_generator: Generator returning training data and label batches
        :type data_generator: neuroracer_ai_sl.utils.ExtractedDataReader
        :param data_processor: SnapshotProcessorSuite to process train/validation data and label batches from
                               train_data_train_data_generator & validation_data_generator
        :type data_processor: neuroracer_ai.suites.SnapshotProcessorSuite
        :param processor_suite: processing suite which processes every input
                for training and prediction. If you need something fancier than
                the predefined ones you can use the
                neuroracer_ai.suites.ManualMappingProcessorSuite. It allows you
                to completely customize the routing of the data to the
                specified processors. You should never implement your own
                ProcessorSuite class! This will cause a failure while loading
                the model next time.
        :type processor_suite: neuroracer_ai.suites.AbstractProcessorSuite
        """
        self.data_generator = data_generator
        self.data_processor = data_processor
        self.processor_suite = processor_suite

    def __len__(self):
        """
        :return: Returns the number of batches per epoch
        :rtype: int
        """
        return self.data_generator.batches_per_epoch

    def __getitem__(self, idx):
        """
        Keras calls this method to get batch with index idx
        :param idx: the index of the wanted batch
        :type idx: int
        :return: x & y values for the model input
        :rtype: tuple<np.array, np.array>
        """
        batch = self.data_generator[idx]
        x, y = self.data_processor.process(batch)
        x = self.processor_suite.process(x)

        return np.array(x), np.array(y)


class KerasModelSL(KerasBaseModel, Trainable, HistoryLogger):
    """
    Keras supervised learning implementation.
    """

    def __init__(self):
        super(KerasModelSL, self).__init__()

    def train(self, train_parameters, checkpoint_path, verbose=0):
        """
        Trains the model with the given data and returns the history of it.

        :param train_parameters: train x data
        :type train_parameters: ai.TrainParameters
        :param checkpoint_path: dir + file name for the model
        :type checkpoint_path: str
        :param verbose: sets the verbosity level of the model 0 = silent,
                1 = progress bar, 2 line per epoch
        :type verbose: int
        :return: History object of the training
        :rtype: Dict
        """

        checkpoint_dir = os.path.split(checkpoint_path)[0]
        cb_tensorboard = k.callbacks.TensorBoard(log_dir=checkpoint_dir,
                                                 write_images=True)
        callbacks = [cb_tensorboard]
        if train_parameters.use_checkpoints:
            if self._is_regression:
                monitor = "val_loss"
                mode = "min"
            else:
                monitor = "val_acc"
                mode = "max"

            callbacks.append(
                k.callbacks.ModelCheckpoint(
                    filepath=os.path.expanduser(checkpoint_path),
                    save_best_only=True,
                    save_weights_only=False,
                    monitor=monitor,
                    mode=mode))

        # NOTE:
        # if we want to use Keras workers, daemon processes can not create child processes
        # so we have to deactivate the extra multiprocessing in the ExtractedDateReader
        train_parameters.train_data_generator.nb_cpu = 1
        train_parameters.validation_data_generator.nb_cpu = 1

        # either use builtin pipeline for framework's defined extracted data format or
        # bypass and use given custom training and evaluation keras.utils.Sequence
        if train_parameters.use_builtin_pipeline:
            train_generator = KerasSequence(train_parameters.train_data_generator,
                                            train_parameters.data_processor,
                                            train_parameters.processor_suite)

            validation_generator = KerasSequence(train_parameters.validation_data_generator,
                                                 train_parameters.data_processor,
                                                 train_parameters.processor_suite)
        else:
            # in this case data_list is not a list but a keras.utils.Sequence
            train_generator = train_parameters.train_data_generator
            validation_generator = train_parameters.validation_data_generator

        history = self._internal_model.fit_generator(
            generator=train_generator,
            steps_per_epoch=train_parameters.steps_per_epoch,
            validation_data=validation_generator,
            validation_steps=train_parameters.validation_steps,
            epochs=train_parameters.epochs,
            callbacks=callbacks,
            verbose=verbose,
            workers=NB_CPU,
            shuffle=train_parameters.shuffle,
            use_multiprocessing=True)

        return history

    def _extract_history_log_params(self, history):
        """
        Extracts the required parameters for the generalized HistoryLogger dict

        :param history: The history object to dump
        """

        # Since keras is the only working backend at the moment, we keep the same structure in general.
        # Maybe this will change in future, so keep the looping structure

        # for i in range(len(history.epoch)):
        #     self.history_log.get('val_loss')[i] = history.history.get('val_loss')[i]
        #     self.history_log.get('val_mean_squared_error')[i] = history.history.get('val_mean_squared_error')[i]
        #     self.history_log.get('loss')[i] = history.history.get('loss')[i]
        #     self.history_log.get('mean_squared_error')[i] = history.history.get('mean_squared_error')[i]

        # since we use their structure directly for the moment
        self.history_log = history.history
