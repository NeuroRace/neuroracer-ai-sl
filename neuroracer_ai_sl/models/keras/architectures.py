import warnings

with warnings.catch_warnings():
    warnings.simplefilter(action="ignore", category=FutureWarning)

    import keras as k
    import keras.initializers as ki
    import keras.layers as kl
    import keras.models as km


class KerasArchitectures:
    """
    Available architectures for keras models.
    """

    def __init__(self):
        pass

    @staticmethod
    def navoshta_nvidia_reg(input_shape, learning_rate=1e-04):
        """
        Behavior Cloning based on NVIDIA approach for less powerful hardware,
        see https://github.com/navoshta/behavioral-cloning

        :param input_shape: TODO
        :param learning_rate: TODO
        :return:
        """

        model = km.Sequential()
        model.add(kl.Conv2D(filters=16, kernel_size=(3, 3), input_shape=input_shape, activation='relu'))
        model.add(kl.MaxPooling2D(pool_size=(2, 2)))
        model.add(kl.Conv2D(filters=32, kernel_size=(3, 3), activation='relu'))
        model.add(kl.MaxPooling2D(pool_size=(2, 2)))
        model.add(kl.Conv2D(filters=64, kernel_size=(3, 3), activation='relu'))
        model.add(kl.MaxPooling2D(pool_size=(2, 2)))
        model.add(kl.Flatten())
        model.add(kl.Dense(units=500, activation='relu'))
        model.add(kl.Dropout(rate=.5))
        model.add(kl.Dense(units=100, activation='relu'))
        model.add(kl.Dropout(rate=.25))
        model.add(kl.Dense(units=20, activation='relu'))
        model.add(kl.Dense(units=1))
        model.compile(optimizer=k.optimizers.Adam(lr=learning_rate), loss='mse', metrics=["mse"])

        return model

    @staticmethod
    def navoshta_nvidia_class(input_shape, learning_rate=1.0):
        """
        Behavior Cloning based on NVIDIA approach for less powerful hardware,
        see https://github.com/navoshta/behavioral-cloning

        :param input_shape: TODO
        :param learning_rate: TODO
        :return:
        """

        model = km.Sequential()
        model.add(kl.Conv2D(filters=16, kernel_size=(3, 3), input_shape=input_shape, activation='relu'))
        model.add(kl.MaxPooling2D(pool_size=(2, 2)))
        model.add(kl.Conv2D(filters=32, kernel_size=(3, 3), activation='relu'))
        model.add(kl.MaxPooling2D(pool_size=(2, 2)))
        model.add(kl.Conv2D(filters=64, kernel_size=(3, 3), activation='relu'))
        model.add(kl.MaxPooling2D(pool_size=(2, 2)))
        model.add(kl.Flatten())
        model.add(kl.Dense(units=500, activation='relu'))
        model.add(kl.Dropout(rate=.5))
        model.add(kl.Dense(units=100, activation='relu'))
        model.add(kl.Dropout(rate=.25))
        model.add(kl.Dense(units=10, activation='softmax'))
        model.compile(loss='categorical_crossentropy',
                      optimizer=k.optimizers.Adadelta(lr=learning_rate),
                      metrics=['accuracy'])

        return model

    @staticmethod
    def obstacle_avoidance_lecun(input_shape, learning_rate=1e-04):
        """
        Obstacle avoidance Multi-Input CNN as designed by Muller et al.
        see http://yann.lecun.com/exdb/publis/pdf/lecun-dave-05.pdf

        :param input_shape:
        :param learning_rate: TODO
        :return:
        """
        in_left = kl.Input(input_shape, name="left_camera_input")
        in_right = kl.Input(input_shape, name="right_camera_input")
        in_both = kl.Concatenate(name="both_camera_inputs")([in_left, in_right])

        conv1 = kl.Conv2D(filters=5, kernel_size=(3, 3), padding="valid",
                          activation="relu", name="conv1")(in_left)

        conv2 = kl.Conv2D(filters=5, kernel_size=(3, 3), padding="valid",
                          activation="relu", name="conv2")(in_right)

        conv3 = kl.Conv2D(filters=20, kernel_size=(3, 3), padding="valid",
                          activation="relu", name="conv3")(in_both)

        concat = kl.Concatenate(name="concatenate")([conv1, conv2, conv3])

        pool1 = kl.MaxPool2D(pool_size=(3, 4), name="pool1")(concat)

        conv4 = kl.Conv2D(filters=96, kernel_size=(5, 3), padding="valid",
                          activation="relu", name="conv4")(pool1)

        pool2 = kl.MaxPool2D(pool_size=(5, 3), name="pool2")(conv4)

        flat = kl.Flatten(name="flatten")(pool2)

        dense1 = kl.Dense(units=100, activation="relu", name="dense1")(flat)

        steer_output = kl.Dense(units=1,
                                name="prediction_steer",
                                bias_initializer="zeros",
                                kernel_initializer=ki.RandomUniform(minval=-3e-4, maxval=3e-4)
                                )(dense1)

        optimizer = k.optimizers.SGD(lr=0.1)
        model = km.Model(inputs=[in_left, in_right], outputs=steer_output)
        model.compile(loss="mse", optimizer=optimizer, metrics=["mse"])

        return model

    @staticmethod
    def dual_view(input_shape, learning_rate=1e-04):
        """
        A Dual-View CNN architecture which processes two images simultaneously.

        :param input_shape: shape of the input images
        :param learning_rate: TODO
        :return: keras.models.Model
        """
        kernel_init = ki.glorot_uniform()
        activity_reg = k.regularizers.l2("0.001")

        in_left = kl.Input(input_shape, name="left_camera_input")
        in_right = kl.Input(input_shape, name="right_camera_input")

        conv1_left = kl.Conv2D(filters=4,
                               kernel_size=(5, 5),
                               padding="valid",
                               activation="relu",
                               name="conv1_left",
                               kernel_initializer=kernel_init
                               )(in_left)

        conv1_right = kl.Conv2D(filters=4,
                                kernel_size=(5, 5),
                                padding="valid",
                                activation="relu",
                                name="conv1_right",
                                kernel_initializer=kernel_init
                                )(in_right)

        pool1_left = kl.MaxPool2D(pool_size=(2, 2), name="pool1_left")(conv1_left)
        pool1_right = kl.MaxPool2D(pool_size=(2, 2), name="pool1_right")(conv1_right)

        conv2_left = kl.Conv2D(filters=8,
                               kernel_size=(3, 3),
                               padding="valid",
                               activation="relu",
                               name="conv2_left",
                               kernel_initializer=kernel_init
                               )(pool1_left)

        conv2_right = kl.Conv2D(filters=8,
                                kernel_size=(3, 3),
                                padding="valid",
                                activation="relu",
                                name="conv2_right",
                                kernel_initializer=kernel_init
                                )(pool1_right)

        pool2_left = kl.MaxPool2D(pool_size=(2, 2), name="pool2_left")(conv2_left)
        pool2_right = kl.MaxPool2D(pool_size=(2, 2), name="pool2_right")(conv2_right)

        flat_left = kl.Flatten()(pool2_left)
        flat_right = kl.Flatten()(pool2_right)

        concat = kl.Concatenate(name="concatenate_left_right_both")([flat_left, flat_right])

        fc_1 = kl.Dense(units=100, activation="tanh", name="fully_con1",
                        activity_regularizer=activity_reg)(concat)

        steer_output = kl.Dense(units=1,
                                name="prediction_steer",
                                activation="tanh",
                                bias_initializer="zeros")(fc_1)

        optimizer = k.optimizers.SGD(lr=0.1)
        model = km.Model(inputs=[in_left, in_right], outputs=steer_output)
        model.compile(loss='mae', optimizer=optimizer,
                      metrics=["mae", 'mse'])

        return model

    @staticmethod
    def nvidia_functional(input_shape, learning_rate=1e-04):
        """
        The architecture which has been used in the End to End Learning for
        Self-Driving Cars paper (https://arxiv.org/abs/1604.07316) from nvidia.

        :param input_shape: TODO
        :type input_shape: Tuple[int, int, int]
        :param learning_rate: TODO
        :return: the constructed model instance
        :rtype: keras.models.Model
        """

        s = kl.Input(shape=input_shape, name="observation_input")

        c1 = kl.Conv2D(filters=24, kernel_size=(5, 5), strides=(2, 2),
                       bias_initializer="he_normal", padding="valid",
                       activation="relu")(s)
        c2 = kl.Conv2D(filters=36, kernel_size=(5, 5), strides=(2, 2),
                       bias_initializer="he_normal", padding="valid",
                       activation="relu")(c1)
        c3 = kl.Conv2D(filters=48, kernel_size=(5, 5), strides=(2, 2),
                       bias_initializer="he_normal", padding="valid",
                       activation="relu")(c2)
        c4 = kl.Conv2D(filters=64, kernel_size=(3, 3), bias_initializer="he_normal",
                       padding="valid",
                       activation="relu")(c3)
        c5 = kl.Conv2D(filters=64, kernel_size=(3, 3), bias_initializer="he_normal",
                       padding="valid",
                       activation="relu")(c4)

        observation_flattened = kl.Flatten(name="flattened_observation")(c5)

        d1 = kl.Dense(units=1164, activation="relu", name="fully_con1",
                      bias_initializer="he_normal")(observation_flattened)
        d1_drop = kl.Dropout(0.2)(d1)
        d2 = kl.Dense(units=100, activation="relu", name="fully_con2",
                      bias_initializer="he_normal")(d1_drop)
        d2_drop = kl.Dropout(0.2)(d2)
        d3 = kl.Dense(units=50, activation="relu", name="fully_con3",
                      bias_initializer="he_normal")(d2_drop)
        d3_drop = kl.Dropout(0.2)(d3)
        d4 = kl.Dense(units=10, activation="relu", name="fully_con4",
                      bias_initializer="he_normal")(d3_drop)
        d4_drop = kl.Dropout(0.2)(d4)

        steer_output = kl.Dense(units=1,
                                activation="tanh",
                                name="prediction_steer",
                                bias_initializer="he_normal",
                                )(d4_drop)
        speed_output = kl.Dense(units=1,
                                activation="sigmoid",
                                name="prediction_speed",
                                bias_initializer="he_normal",
                                )(d4_drop)

        merged_output = kl.concatenate([steer_output, speed_output],
                                       name="merge_concatenate_predictions")

        model = km.Model(inputs=s, outputs=merged_output)
        model.compile(loss="mse", optimizer="adam", metrics=["mse"])

        return model

    @staticmethod
    def nvidia(input_shape, learning_rate=1e-04):
        """
        The architecture which has been used in the End to End Learning for
        Self-Driving Cars paper (https://arxiv.org/abs/1604.07316) from nvidia.

        :param input_shape: TODO
        :type input_shape: Tuple[int, int, int]
        :param learning_rate: TODO
        :return: the constructed model instance
        :rtype: keras.models.Model
        """

        model = km.Sequential()

        # Convolution no.1
        model.add(
            kl.Conv2D(filters=24, kernel_size=(5, 5), strides=(2, 2),
                      bias_initializer="he_normal", padding="valid",
                      input_shape=input_shape, activation="relu"))

        # Convolution no.2
        model.add(
            kl.Conv2D(filters=36, kernel_size=(5, 5), strides=(2, 2),
                      bias_initializer="he_normal", padding="valid",
                      activation="relu"))

        # Convolution no.3
        model.add(
            kl.Conv2D(filters=48, kernel_size=(5, 5), strides=(2, 2),
                      bias_initializer="he_normal", padding="valid",
                      activation="relu"))

        # Convolution no.4
        model.add(
            kl.Conv2D(filters=64, kernel_size=(3, 3), bias_initializer="he_normal",
                      padding="valid", activation="relu"))

        # Convolution no.5
        model.add(
            kl.Conv2D(filters=64, kernel_size=(3, 3), bias_initializer="he_normal",
                      padding="valid", activation="relu"))
        model.add(kl.Flatten())

        # Fully connected no.1
        model.add(kl.Dense(units=1164, bias_initializer="he_normal", activation="relu"))
        model.add(kl.Dropout(0.2))

        # Fully connected no.2
        model.add(kl.Dense(units=100, bias_initializer="he_normal", activation="relu"))
        model.add(kl.Dropout(0.2))

        # Fully connected no.3
        model.add(kl.Dense(units=50, bias_initializer="he_normal", activation="relu"))

        # Fully connected no.4
        model.add(kl.Dense(units=10, bias_initializer="he_normal", activation="tanh"))

        # Fully connected no.5
        model.add(kl.Dense(units=1, bias_initializer="he_normal", activation="tanh",
                           name="prediction"))

        model.compile(loss="mse", optimizer="adam", metrics=["mse"])

        return model

    @staticmethod
    def small_net(input_shape, learning_rate=1e-04):
        """
        Self driving approach with a small network

        :param input_shape: TODO
        :type input_shape: Tuple[int, int, int]
        :param learning_rate: TODO
        :return: the constructed model instance
        :rtype: keras.models.Model
        """

        model = km.Sequential()

        model.add(kl.Conv2D(
            filters=8,
            kernel_size=(7, 7),
            activation="relu",
            name="conv1",
            kernel_initializer="glorot_uniform",
            input_shape=input_shape,
            strides=2,
            kernel_regularizer=k.regularizers.l1(0.008)))

        model.add(kl.BatchNormalization())

        model.add(kl.Conv2D(
            filters=16,
            kernel_size=(5, 5),
            activation="relu",
            name="conv2",
            kernel_initializer="glorot_uniform",
            kernel_regularizer=k.regularizers.l1(0.008)))

        model.add(kl.BatchNormalization())

        model.add(kl.Conv2D(
            filters=16,
            kernel_size=(5, 5),
            activation="relu",
            name="conv3",
            kernel_initializer="glorot_uniform",
            kernel_regularizer=k.regularizers.l1(0.002)))

        model.add(kl.Flatten(name="flattened_observation"))

        model.add(kl.Dense(
            units=150, activation="relu", kernel_initializer="glorot_uniform"))

        model.add(kl.Dropout(0.2))

        model.add(kl.Dense(
            units=80, activation="relu", kernel_initializer="glorot_uniform"))

        model.add(kl.Dense(
            units=1, kernel_initializer="glorot_uniform", name="prediction"))

        model.compile(loss="mse", optimizer="adam", metrics=["mse"])

        return model

    @staticmethod
    def small_net2(input_shape, learning_rate=1e-04):
        """
        Self driving approach with a small network

        :param input_shape: TODO
        :type input_shape: Tuple[int, int, int]
        :param learning_rate: TODO
        :return: the constructed model instance
        :rtype: keras.models.Model
        """

        model = km.Sequential()

        model.add(kl.Conv2D(
            filters=8,
            kernel_size=(7, 7),
            activation="relu",
            name="conv1",
            kernel_initializer="glorot_uniform",
            input_shape=input_shape,
            strides=2,
            kernel_regularizer=k.regularizers.l1(0.008)))

        model.add(kl.BatchNormalization())

        model.add(kl.Conv2D(
            filters=16,
            kernel_size=(5, 5),
            activation="relu",
            name="conv2",
            kernel_initializer="glorot_uniform",
            kernel_regularizer=k.regularizers.l1(0.008)))

        model.add(kl.BatchNormalization())

        model.add(kl.Conv2D(
            filters=16,
            kernel_size=(5, 5),
            activation="relu",
            name="conv3",
            kernel_initializer="glorot_uniform",
            kernel_regularizer=k.regularizers.l1(0.002)))

        model.add(kl.Flatten(name="flattened_observation"))

        model.add(kl.Dense(
            units=250, activation="relu", kernel_initializer="glorot_uniform"))

        model.add(kl.Dropout(0.2))

        model.add(kl.Dense(
            units=120, activation="relu", kernel_initializer="glorot_uniform"))

        model.add(kl.Dense(
            units=1, kernel_initializer="glorot_uniform", name="prediction"))

        model.compile(loss="mse", optimizer="adam", metrics=["mse"])

        return model
