"""
Contains the AbstractModel interface and all implementations of it. Available
architectures for the models are also in here. This servers as a abstraction
from the used framework.
"""

from neuroracer_ai.factories.abstract import EBackends
from neuroracer_ai.utils.backend_solver import get_backend

imports = []

_BACKEND = get_backend()

if _BACKEND == EBackends.KERAS:
    try:
        from neuroracer_ai_sl.models.keras.architectures import KerasArchitectures
        from neuroracer_ai_sl.models.keras_backend_sl import KerasModelSL, KerasSequence

        imports.append("KerasArchitectures")
        imports.append("KerasModelSL")
        imports.append("KerasSequence")
    except ImportError as e:
        print("Required module for defined backend Keras not found:", e)

elif _BACKEND == EBackends.PYTORCH:
    try:
        from neuroracer_ai_sl.models.pytorch_backend_sl import PyTorchModelSL, PyTorchArchitectures

        imports.append("PyTorchModelSL")
        imports.append("PyTorchArchitectures")
    except ImportError as e:
        print("Required module for defined backend PyTorch not found:", e)

else:
    raise NotImplementedError('Define backend not found: ', _BACKEND)

__all__ = imports
