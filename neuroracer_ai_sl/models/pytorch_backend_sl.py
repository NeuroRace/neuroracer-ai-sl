from __future__ import division, print_function

import numpy as np
import torch as t
from neuroracer_ai.models.abstract import Trainable
from neuroracer_ai.models.pytorch_base_backend import PyTorchBaseModel, CUDA
from neuroracer_ai.utils.config_handler import NB_CPU
from torch.utils.data import Dataset, DataLoader

from neuroracer_ai_sl.models.pytorch.navoshta import NavoshtaNetRegression, NavoshtaNetClassification
from neuroracer_ai_sl.utils.history_logger import HistoryLogger, LOG_STRUCTURE

t.set_default_dtype(t.float)


class DatasetSL(Dataset):
    def __init__(self, data_generator, data_processor, processor_suite):
        """
        :param data_generator: Generator returning training data and label batches
        :type data_generator: neuroracer_ai_sl.utils.ExtractedDataReader
        :param data_processor: SnapshotProcessorSuite to process train/validation data and label batches from
                               train_data_train_data_generator & validation_data_generator
        :type data_processor: neuroracer_ai.suites.SnapshotProcessorSuite
        :param processor_suite: processing suite which processes every input
                for training and prediction. If you need something fancier than
                the predefined ones you can use the
                neuroracer_ai.suites.ManualMappingProcessorSuite. It allows you
                to completely customize the routing of the data to the
                specified processors. You should never implement your own
                ProcessorSuite class! This will cause a failure while loading
                the model next time.
        :type processor_suite: neuroracer_ai.suites.AbstractProcessorSuite
        """

        self.data_generator = data_generator
        self.data_processor = data_processor
        self.processor_suite = processor_suite

    def __getitem__(self, idx):
        """
        TODO

        :param idx: TODO
        :return: TODO
        """

        batch = self.data_generator[idx]
        x, y = self.data_processor.process(batch)
        x = self.processor_suite.process(x)

        # Note:
        # PyTorch requires different shape than internal generator provides,
        # so drop one dimension of x and y, since each has an additional one. This is by design,
        # since the internal generator is capable of producing batches of processed data.

        return np.squeeze(x, axis=0), np.squeeze(y, axis=0)

    def __len__(self):
        """
        Returns amount of total snapshots for each generator

        :return: TODO
        """

        return self.data_generator.n_total_data


class PyTorchModelSL(PyTorchBaseModel, Trainable, HistoryLogger):
    """
    PyTorch supervised learning implementation.
    """

    def __init__(self):
        super(PyTorchModelSL, self).__init__()
        self._verbose = 0

    def train(self, train_parameters, checkpoint_path, verbose=0):
        """
        Trains the model with the given data and returns the history of it.

        :param train_parameters: train x data
        :type train_parameters: ai.TrainParameters
        :param checkpoint_path: dir + file name for the model
        :type checkpoint_path: str
        :param verbose: sets the verbosity level of the model 0 = silent,
                1 = progress bar, 2 line per epoch
        :type verbose: int
        :return: History object of the training
        :rtype: Dict
        """

        self._verbose = verbose

        # NOTE:
        # if we want to use PyTorch workers, daemon processes can not create child processes
        # so we have to deactivate the extra multiprocessing in the ExtractedDateReader
        train_parameters.train_data_generator.nb_cpu = 1
        train_parameters.validation_data_generator.nb_cpu = 1
        # also we have to set the internal generator batch size to 1,
        # since PyTorch wants one item a time and creates batches itself
        # instead of accepting complete batches like keras
        train_parameters.train_data_generator.batch_size = 1
        train_parameters.validation_data_generator.batch_size = 1

        # multiprocessing (and cuda if found)
        pin_memory = True if self._device_to_use == CUDA else False
        kwargs = {'num_workers': NB_CPU, 'pin_memory': pin_memory}

        # either use builtin pipeline for framework's defined extracted data format or
        # bypass and use given custom training and evaluation torch.utils.data.DataSet
        if train_parameters.use_builtin_pipeline:
            dataset_train = DatasetSL(train_parameters.train_data_generator,
                                      train_parameters.data_processor,
                                      train_parameters.processor_suite)
            dataset_eval = DatasetSL(train_parameters.validation_data_generator,
                                     train_parameters.data_processor,
                                     train_parameters.processor_suite)
        else:
            dataset_train = train_parameters.train_data_generator
            dataset_eval = train_parameters.validation_data_generator

        # get generator for training data
        train_loader = DataLoader(dataset=dataset_train,
                                  batch_size=train_parameters.batch_size,
                                  shuffle=train_parameters.shuffle,
                                  drop_last=False,
                                  **kwargs)

        # get generator for evaluation data
        eval_loader = DataLoader(dataset=dataset_eval,
                                 batch_size=train_parameters.batch_size,
                                 shuffle=train_parameters.shuffle,
                                 drop_last=False,
                                 **kwargs)

        # prepare belongings
        self._optimizer = self._internal_model.get_optimizer()
        self._criterion = self._internal_model.get_loss_function()

        # logging
        log = LOG_STRUCTURE

        # init best validation loss by a hugh number
        val_loss_best = 10000.0
        # init best accuracy loss by zero
        val_acc_best = 0.0

        # loop over epochs
        for epoch in range(train_parameters.epochs):
            if self._verbose > 0:
                print('\nEpoch {}/{}'.format(epoch + 1, train_parameters.epochs))

            # train
            loss, acc_mse = self._train(train_loader)

            # validation
            val_loss, val_acc_mse = self._eval(eval_loader)

            # check if validation value has improved
            if self._is_regression:
                # regression
                model_improved = True if val_loss < val_loss_best else False
            else:
                # classification
                # TODO improve saving (catch over- and underfitting
                model_improved = True if val_loss < val_loss_best or val_acc_mse > val_acc_best else False

            # save model if has improved validation value
            if model_improved:
                val_loss_best = val_loss

                if not self._is_regression:
                    # classification
                    val_acc_best = val_acc_mse

                self.save(model_path=checkpoint_path)

            # logging
            log.get('val_loss').append(val_loss)
            log.get('loss').append(loss)

            if self._is_regression:
                log.get('val_mean_squared_error').append(val_acc_mse)
                log.get('mean_squared_error').append(acc_mse)
            else:
                log.get('val_acc').append(val_acc_mse)
                log.get('acc').append(acc_mse)

        return log

    def _train(self, train_loader):
        """
        Training process

        :param train_loader:
        :return:
        """

        # set PyTorch model to training mode
        self._internal_model.train()

        _loss = 0.0
        _acc_mse = 0.0

        for data, target in train_loader:
            # TODO probably could be more generic?
            # set type for image
            generic_data = data.float()
            # type change required
            if self._is_regression:
                # regression
                generic_target = target.float()
            else:
                # classification
                generic_target = target.long()

            data, target = generic_data.to(device=self._device_to_use), generic_target.to(device=self._device_to_use)

            # computations
            self._optimizer.zero_grad()

            output = self._internal_model(data)

            loss = self._criterion(output, target)
            loss.backward()

            self._optimizer.step()

            if self._is_regression:
                # regression
                _loss += loss.item() * data.size(0)  # sum up batch loss

                # TODO  why is mse negative?
                #  loss is identical to keras and networks act identical on test data and in simulation
                # _acc_mse += output.mean().item()  # sum up batch mse

                # TODO probably remove
                # use loss instead like keras
                _acc_mse = _loss
            else:
                # classification
                _loss += loss.item()  # sum up batch loss
                output = output.argmax(dim=1, keepdim=True)  # get the index of the max log-probability
                _acc_mse += output.eq(target.view_as(output)).sum().item()  # sum up batch acc

        _loss /= len(train_loader.dataset)
        _acc_mse /= len(train_loader.dataset)

        if self._verbose > 0:
            print('loss: {}'.format(_loss))

            if self._is_regression:
                print('mse: {}'.format(_acc_mse))
            else:
                print('acc: {}'.format(_acc_mse))

        return _loss, _acc_mse

    def _eval(self, eval_loader):
        """
        Validation process

        :param eval_loader:
        :return:
        """

        # set PyTorch model to validation mode (disable dropout, etc.)
        self._internal_model.eval()

        val_loss = 0.0
        val_acc_mse = 0.0

        with t.no_grad():
            for data, target in eval_loader:
                # TODO more generic?
                # set type for image
                generic_data = data.float()
                # type change required
                if self._is_regression:
                    # regression
                    generic_target = target.float()
                else:
                    # classification
                    generic_target = target.long()

                data, target = generic_data.to(device=self._device_to_use), generic_target.to(
                    device=self._device_to_use)

                output = self._internal_model(data)

                if self._is_regression:
                    # regression
                    val_loss += self._criterion(output, target).item() * data.size(0)  # sum up batch loss

                    # TODO  why is mse negative?
                    #  loss is identical to keras and networks act identical on test data and in simulation
                    # val_acc_mse += output.mean().item()  # sum up batch mse

                    # TODO probably remove
                    # use loss instead like keras
                    val_acc_mse = val_loss
                else:
                    # classification
                    val_loss += self._criterion(output, target).item()  # sum up batch loss
                    output = output.argmax(dim=1, keepdim=True)  # get the index of the max log-probability
                    val_acc_mse += output.eq(target.view_as(output)).sum().item()  # sum up batch acc

        val_loss /= len(eval_loader.dataset)
        val_acc_mse /= len(eval_loader.dataset)

        if self._verbose > 0:
            print('val_loss: {}'.format(val_loss))

            if self._is_regression:
                print('val_mse: {}'.format(val_acc_mse))
            else:
                print('val_acc: {}'.format(val_acc_mse))

        return val_loss, val_acc_mse

    def _extract_history_log_params(self, history):
        """
        Extracts the required parameters for the generalized HistoryLogger dict

        :param history: The history object to dump
        """

        # We orientate on keras history since it was the first implemented backend and we don't have to change anything.
        # Maybe this will be modified in future, so keep looping example.

        # for i in range(0, history.get('epochs')):
        #     self.history_log.get('val_loss').append(history.get('val_loss')[i])
        #     self.history_log.get('val_mean_squared_error').append(history.get('val_mean_squared_error')[i])
        #     self.history_log.get('loss').append(history.get('loss')[i])
        #     self.history_log.get('mean_squared_error').append(history.get('mean_squared_error')[i])

        # since we use their structure directly for the moment
        self.history_log = history


class PyTorchArchitectures:
    """
    Available architectures for PyTorch models. We keep it called architecture functions even it is torch.nn.Module,
    to keep abstraction expressions homogeneous in the framework.

    All functions have to be wrappers.
    """

    def __init__(self):
        pass

    @staticmethod
    def navoshta_nvidia_reg(input_shape, learning_rate=1e-04):
        """
        Behavior cloning based on NVIDIA approach for less powerful hardware,
        see https://github.com/navoshta/behavioral-cloning

        :param input_shape: TODO
        :param learning_rate: TODO
        :return:
        """

        return NavoshtaNetRegression(input_shape=input_shape, learning_rate=learning_rate)

    @staticmethod
    def navoshta_nvidia_class(input_shape, learning_rate=1.0):
        """
        Behavior cloning based on NVIDIA approach for less powerful hardware,
        see https://github.com/navoshta/behavioral-cloning

        :param input_shape: TODO
        :param learning_rate: TODO
        :return:
        """

        return NavoshtaNetClassification(input_shape=input_shape, learning_rate=learning_rate)
