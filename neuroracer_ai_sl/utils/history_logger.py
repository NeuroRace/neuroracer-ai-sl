from __future__ import print_function

from abc import abstractmethod
from inspect import getsource
from os.path import isfile
from time import strftime

from neuroracer_ai.utils.backend_solver import get_backend

"""
The list indices of each dict index are the epochs' numbers.

Example:

epochs = 3

to make easy, same value for all variables in each epoch is used:

1 = 0.1
2 = 0.2
3 = 0.3

LOG_STRUCTURE = {
    'val_loss': [0.1, 0.2, 0.3],
    'val_mean_squared_error': [0.1, 0.2, 0.3],
    'val_acc': [0.1, 0.2, 0.3],
    'loss': [0.1, 0.2, 0.3],
    'mean_squared_error': [0.1, 0.2, 0.3],
    'acc': [0.1, 0.2, 0.3]
}
"""
# TODO actually only loss is shared, acc/mse should be generic by is_regression attribute of model somehow
LOG_STRUCTURE = {
    'val_loss': [],
    'val_mean_squared_error': [],
    'val_acc': [],
    'loss': [],
    'mean_squared_error': [],
    'acc': []
}


class HistoryLogger:
    """
    Logs information from training. Inheriting class only require to fill predefined structure
    inside _extract_history_log_params().
    """

    def __init__(self):
        """
        Initializes the logger dictionary for SL. See _init_history_logger() for structure definition

        """

        self.history_log = LOG_STRUCTURE

    @abstractmethod
    def _extract_history_log_params(self, history):
        """
        This method is backend based.

        In your child class, for each epoch, fill the pre defined structure of the list 'history_log' with values.
        The list indices of each dict index are the epochs' numbers.

        Example:

        epochs = 3

        to make easy, same value for all variables in each epoch is used:

        1 = 0.1
        2 = 0.2
        3 = 0.3

        self.history_log = {
            'val_loss': [0.1, 0.2, 0.3],
            'val_mean_squared_error': [0.1, 0.2, 0.3],
            'val_acc': [0.1, 0.2, 0.3],
            'loss': [0.1, 0.2, 0.3],
            'mean_squared_error': [0.1, 0.2, 0.3],
            'acc': [0.1, 0.2, 0.3]
        }

        :param history: TODO
        :return: TODO
        """
        pass

    def _table_from_dict(self):
        """
        Creates a list of strings of a table with one column for each key.

        :return: List of strings of a table.
        :rtype: list[str]
        """

        # TODO remove once LOG_STRUCTURE or this function is generic
        if 'val_mean_squared_error' in self.history_log and self.history_log['val_mean_squared_error']:
            if 'val_acc' in self.history_log:
                del (self.history_log['val_acc'])
            if 'acc' in self.history_log:
                del (self.history_log['acc'])
        else:
            if 'val_mean_squared_error' in self.history_log:
                del (self.history_log['val_mean_squared_error'])
            if 'mean_squared_error' in self.history_log:
                del (self.history_log['mean_squared_error'])

        clean_dict = {}

        longest_values = []

        number_rows = 0

        for key, value in self.history_log.items():
            # key type check
            if type(key) is not str:
                continue

            # iterable check
            try:
                iterator = iter(value)
            except TypeError:
                continue

            i_len = len(value)
            if i_len > number_rows:
                number_rows = i_len

            longest_value = len(key)
            for i in iterator:
                s_len = len(str(i))
                if s_len > longest_value:
                    longest_value = s_len
            longest_values.append(longest_value)
            clean_dict[key] = value

        table_width = 0
        for i in longest_values:
            table_width += (i + 3)

        line = ("{0:-^" + str(table_width) + "}").format("")

        table = []

        row = ""

        for key, longest_value in zip(clean_dict.keys(), longest_values):
            row = row + ("{:^" + str(longest_value + 2) + "}|").format(key)

        table.append(row[:-1])
        table.append(line)

        for i in range(number_rows):
            row = ""
            for v, longest_value in zip(clean_dict.values(), longest_values):
                row = row + ("{:^" + str(longest_value + 2) + "}|").format(v[i])
            table.append(row[:-1])

        return table

    def log_history(self, history, architecture_func, model_name, destination):
        """
        Unifies the output based on specified variables, so output is backend independent.

        :param history: TODO
        :param architecture_func: TODO
        :param model_name: TODO
        :param destination: TODO
        :return:
        """

        self._extract_history_log_params(history=history)

        stats = self._table_from_dict()

        data = ["backend:", get_backend(), "",
                "model_name:", model_name, "",
                "timestamp:", strftime("%d-%m-%Y %H:%M:%S"), "",
                "metrics:", ""]
        data.extend(stats)

        # get source code of the architecture
        if architecture_func is not None:
            architecture_code = getsource(architecture_func)
            data.append("\narchitecture:")
            data.append(architecture_code)

        if isfile(destination):
            with open(destination, "a") as f:
                f.write(
                    "\n- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n\n")
                for line in data:
                    f.write(line)
                    f.write("\n")
        else:
            with open(destination, "w") as f:
                for line in data:
                    f.write(line)
                    f.write("\n")
