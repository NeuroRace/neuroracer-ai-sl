#!/usr/bin/env python

import logging
import math
import operator
import os
import sys
from functools import partial
from multiprocessing import Pool

import cv2
import numpy as np
from neuroracer_ai.utils.abstract import AbstractGenerator
from neuroracer_ai.utils.snapshot import ImageSnapshot

try:
    import rosbag
    from cv_bridge import CvBridge
    from rospy_message_converter.message_converter import convert_ros_message_to_dictionary
except ImportError:
    raise ImportError(
        "No ROS version found. RosbagReader is not available. To enable support, install ROS for your distribution and rospy_message_converter>=0.3.0 via pypi.")

logger = logging.getLogger("pymp")
logger.addHandler(logging.StreamHandler(sys.stdout))

IMAGE_FILE_EXTENSION = ".jpg"
cv_bridge = CvBridge()


class RosbagReader(AbstractGenerator):

    def __init__(self, bag_sources, rosbag_batch_size, shuffle=False, camera_topics=None,
                 action_topics=None, nb_cpu=1, frame_skip=0, return_dict=False,
                 loop_indefinitely=True, delta_t=5e7):
        """
        Reads all rosbags in the specified directories and yields
        SnapshotImage object batches created from the data within the rosbag.
        A SnapshotImage object can have multiple images (when multiple camera
        topics are set) and multiple actions (when multiple action topics are
        set) associated with it. Since not every single camera frame has a
        direct action associated with it, an algorithm is used to match
        actions to images within a certain time delta.

        :param bag_sources: list of sources from where to read bags
                            (path to bag files or path to directories, which
                            contain bag files)
        :type bag_sources: list[str]
        :param rosbag_batch_size: How many rosbags should be processed in
                                  every call to the generator. The Batch size
                                  referes NOT to the amount of snapshots
                                  being returned
        :type rosbag_batch_size: int
        :param action_topics: The action topics to read from the bag
        :type action_topics: list[str]
        :param camera_topics: The camera topics to read from the bag.
        :type camera_topics: list[str]
        :param frame_skip: Number of frames to skip after reading a frame.
                           frame_skip=0: take all frames
                           frame_skip=1: take every second frame
        :type frame_skip: int
        :param nb_cpu: The number of cpus to use. nb_cpu > 1 leads to
                       multiprocessing
        :type nb_cpu: int
        :param return_dict: When True, group the snapshot objects by folder
        :type return_dict: bool
        :param loop_indefinitely: When True, the generator loops around and
                                  yields batches forever
        :type loop_indefinitely: bool
        :param delta_t: The maximum time delta for two messages to be
                        considered correlated, in nanosecs
        :type delta_t: int
        :returns: A list of snapshot objects.
        """

        super(RosbagReader, self).__init__()
        self.bag_sources = bag_sources
        self.rosbag_batch_size = rosbag_batch_size
        self.shuffle = shuffle
        self.camera_topics = camera_topics
        self.action_topics = action_topics
        self.nb_cpu = nb_cpu
        self.frame_skip = frame_skip
        self.return_dict = return_dict
        self.loop_indefinitely = loop_indefinitely
        self.delta_t = delta_t
        self.batches_per_epoch = None  # needed for keras model.fit_generator
        self.n_total_bags = None  # convenience attribute
        self.rosbag_paths = None  # convenience attribute
        self._check_init_params()
        self._prepare_generator()

    def _prepare_generator(self):
        # we want the "batches_per_epoch" and "rosbag_paths" variables set before the first call to "next"
        self.rosbag_paths = sorted(self._get_rosbag_paths(self.bag_sources))
        self.n_total_bags = len(self.rosbag_paths)
        self.batches_per_epoch = int(math.ceil(float(self.n_total_bags) / float(self.rosbag_batch_size)))

    def _check_init_params(self):
        if self.camera_topics is None or len(self.camera_topics) == 0:
            raise ValueError("camera_topics are None or empty")

        if self.action_topics is None or len(self.action_topics) == 0:
            raise ValueError("action_topic are None or empty")

    @staticmethod
    def _get_rosbag_paths(source_paths):
        """
        Returns a list of all rosbags found in the source_paths.

        :param source_paths: A list containing paths to rosbags or paths to
                             directories containing rosbags.
        :returns: A list of paths to all .bag files specified by sources or
                  found in a directory in sources.
        """

        bag_paths = set()
        for source_path in source_paths:
            if os.path.isfile(source_path):
                bag_paths.add(source_path)
            elif os.path.isdir(source_path):
                files = [os.path.join(source_path, file_)
                         for (_, _, files) in os.walk(source_path) for file_ in files
                         if file_.endswith(".bag")]
                bag_paths = bag_paths | set(files)
            else:
                raise ValueError(" No such file or directory: {}.".format(source_path))
        return list(bag_paths)

    @staticmethod
    def _flatten_snapshot_lists(snapshot_batch):
        return [snap for snap_list in snapshot_batch for snap in snap_list]

    @staticmethod
    def _snapshot_lists_to_dict(rosbag_paths_batch, snapshot_batch):
        return {bag_path: snap_list
                for bag_path, snap_list
                in zip(rosbag_paths_batch, snapshot_batch)}

    def _index_needs_reset(self):
        return self.loop_indefinitely and (self.index + self.rosbag_batch_size >= self.n_total_bags)

    def send(self):
        while self.index < self.n_total_bags:
            if self.index == 0 and self.shuffle:
                np.random.shuffle(self.rosbag_paths)

            rosbag_paths_batch = self.rosbag_paths[self.index:self.index + self.rosbag_batch_size]
            self.index = 0 if self._index_needs_reset() else self.index + self.rosbag_batch_size

            if self.nb_cpu == 1:
                snapshot_batch = [_read_rosbag(path_to_rosbag=bag,
                                               camera_topics=self.camera_topics,
                                               action_topics=self.action_topics,
                                               frame_skip=self.frame_skip,
                                               delta_t=self.delta_t)
                                  for bag in rosbag_paths_batch]
            else:
                _read_rosbag_multiprocessing = partial(_read_rosbag,
                                                       camera_topics=self.camera_topics,
                                                       action_topics=self.action_topics,
                                                       frame_skip=self.frame_skip,
                                                       delta_t=self.delta_t)
                pool = Pool(processes=self.nb_cpu)
                snapshot_batch = pool.imap(_read_rosbag_multiprocessing, rosbag_paths_batch)
                pool.close()
                pool.join()

            if self.return_dict:
                return self._snapshot_lists_to_dict(rosbag_paths_batch, snapshot_batch)
            else:
                return self._flatten_snapshot_lists(snapshot_batch)

        # raise StopIteration when generator is exhausted
        self.throw()


def _read_rosbag(path_to_rosbag, camera_topics, action_topics, frame_skip, delta_t):
    """
    Reads a rosbag and extracts all messages published to the topics specified
    in camera_topics and action_topics. Since not every single camera frame
    has a direct action associated with it, an algorithm is used to match
    actions to images within a certain time delta. The function returns
    Snapshot objects to ease further processing.
    ATTENTION: This function must be declared outside of the RosBagReader
    class, otherwise multiprocessing will not work. Functions passed to a
    multiprocessing pool must be serializable, which class and instance
    methods are not in Python 2.7

    :param path_to_rosbag: filepath to rosbag file
    :type path_to_rosbag: str
    :param action_topics: The action topics to read from the bag
    :type action_topics: list[str]
    :param camera_topics: The camera topics to read from the bag.
    :type camera_topics: list[str]
    :param frame_skip: Number of frames to skip after reading a frame.
                           frame_skip=0: read all frames
                           frame_skip=1: read every second frame
    :type frame_skip: int
    :param delta_t: The maximum time delta for two messages to be considered
                    correlated, in nanosecs
    :type delta_t: int
    :return: ImageSnapshot
    """

    try:
        bag = rosbag.Bag(path_to_rosbag)
    except rosbag.bag.ROSBagUnindexedException:
        raise IOError("Unindexed bag '{}'".format(path_to_rosbag))

    # camera messages, especially ones for zed stereo images, should be
    # stamped so the left + right (and depth) images can be grouped together
    # by time. They will have the same header-timestamp (!but NOT the same
    # rosbag timestamp!)
    # values are tupels of (topic, cv2image)
    camera_messages_grouped_by_timestamp = {}

    # group action messages by topic
    # messages might not be stamped, so timestamp might come from rosbag
    # values are tupels of (timestamp, msg)
    action_messages_grouped_by_topic = {}

    all_topics = list(camera_topics) + list(action_topics)

    for topic, msg, rosbag_time in bag.read_messages(topics=all_topics):
        timestamp = rosbag_time
        if topic in camera_topics:
            compressed_cv2_image = _img_msg_to_cv2(msg)
            try:
                camera_messages_grouped_by_timestamp[timestamp].append(
                    (topic, compressed_cv2_image))
            except KeyError:
                camera_messages_grouped_by_timestamp[timestamp] = [
                    (topic, compressed_cv2_image)]

        elif topic in action_topics:
            try:
                action_messages_grouped_by_topic[topic].append((timestamp, msg))
            except KeyError:
                action_messages_grouped_by_topic[topic] = [(timestamp, msg)]

    bag.close()
    snapshots = _create_snapshots(camera_messages_grouped_by_timestamp,
                                  action_messages_grouped_by_topic,
                                  camera_topics,
                                  frame_skip,
                                  delta_t)
    return snapshots


def _img_msg_to_cv2(image_msg):
    """
    ATTENTION: This function must be declared outside of the RosBagReader
    class, otherwise multiprocessing will not work. Functions passed to a
    multiprocessing pool must be serializable, which class and instance
    methods are not in Python 2.7
    """

    cv_image = cv_bridge.compressed_imgmsg_to_cv2(image_msg)
    cv_image_compressed = cv2.imencode(IMAGE_FILE_EXTENSION, cv_image)[1]
    return cv_image_compressed


def _create_snapshots(cam_msgs_by_timestamp, action_msgs_by_topic, camera_topics, frame_skip, delta_t):
    """
    Takes camera and action messages, finds closest action messages for as
    much camera messages as possible and then creates ImageSnapshot objects
    for all valid messages.

    ATTENTION: This function must be declared outside of the RosBagReader
    class, otherwise multiprocessing will not work. Functions passed to a
    multiprocessing pool must be serializable, which class and instance
    methods are not in Python 2.7

    :param cam_msgs_by_timestamp: camera messages grouped by timestamp.
                                  eg. zed/left and zed/right will have the
                                  same timestamp
    :type cam_msgs_by_timestamp: dict[rospy.rostime.Time: tuple[str, np.array]]
    :param action_msgs_by_topic: action timestamps and action messages,
                                 grouped by topic
    :type action_msgs_by_topic: dict[str: tuple[rospy.rostime.Time, ros_msg]]
    :param delta_t: The maximum time delta for two messages to be considered
                    correlated, in nanosecs
    :type delta_t: int
    :return: ImageSnapshot objects
    :rtype: list[ImageSnapshot]
    """

    snapshots = []
    skip_counter = 0
    for timestamp, topic_image_tuples in sorted(cam_msgs_by_timestamp.items()):
        if skip_counter == 0:
            closest_actions = _find_actions_close_to_timestamp(timestamp, action_msgs_by_topic, delta_t)

            # only create snapshots when a msg for every action type
            # could be found close to camera events
            if (len(closest_actions) == len(action_msgs_by_topic.keys())
                    and len(topic_image_tuples) == len(camera_topics)):
                camera_msgs = {topic: image for topic, image in topic_image_tuples}
                action_msgs = {topic: convert_ros_message_to_dictionary(action)
                               for topic, action in closest_actions}

                snapshot = ImageSnapshot(camera_msgs=camera_msgs,
                                         action_msgs=action_msgs,
                                         timestamp=timestamp)
                snapshots.append(snapshot)
        skip_counter = (skip_counter + 1) % (frame_skip + 1)
    return snapshots


def _find_actions_close_to_timestamp(timestamp, action_messages_by_topic, delta_t=5e7):
    """
    group camera msgs + action msgs together to make snapshots.
    camera msgs are already grouped by header-timestamp, but action msgs may
    not have a header. In this case we use the rosbag timestamp, which will be
    slightly delayed. If a message header is present for the action messages,
    it will be used instead. When looking for close actions, small time deltas
    are of course prefered, while actions in the future are prefered over
    actions in the past. For example a steering action to the left shortly
    after an image was taken is prefered over a steering action forwards with
    the same time delta, but recorded before the image was taken.

    delta_t: 0.05s -> 82% of image(s) are associated with an action
    delta_t: 0.09s -> 90% of image(s) are associated with an action
    delta_t: 0.20s -> 95% of image(s) are associated with an action
    delta_t: 0.35s -> 100% of image(s) are associated with an action

    ATTENTION: This function must be declared outside of the RosBagReader
    class, otherwise multiprocessing will not work.  Functions passed to a
    multiprocessing pool must be serializable, which class and instance
    methods are notin Python 2.7

    :param timestamp: Reference timestamp, search for actions near this time
    :type timestamp: rospy.rostime.Time
    :param action_messages_by_topic: action timestamps and action messages,
                                     grouped by topic
    :type action_messages_by_topic: dict[str: (rospy.rostime.Time, ros_msg)]
    :param delta_t: time delta in nanoseconds. only actions <= delta are
                    matched to camera events
    :type delta_t: int
    :return: closest action topics + action messages
    :rtype: list[str, ros_msg]
    """
    close_actions = []
    for topic, time_and_action_messages in action_messages_by_topic.items():

        candidates = []
        msgs_sorted_by_time = sorted(time_and_action_messages, key=operator.itemgetter(0))

        for action_timestamp, action_message in msgs_sorted_by_time:
            delta_time = abs(timestamp - action_timestamp).to_nsec()
            if delta_time <= delta_t:
                candidates.append((delta_time, action_message))

        # sort by smallest absolute delta_time and prefer negative deltas
        # over postive deltas (action slightly after image is better than
        # action slightly before image, especially when actions use the
        # inaccurate rosbag timestamp)
        if candidates:
            candidates = sorted(sorted(candidates), key=lambda delta_msg: abs(delta_msg[0]))
            closest_delta, closest_action_msg = candidates[0]
            close_actions.append((topic, closest_action_msg))

    return close_actions
