"""
This module defines functions to augment images and actions
"""

import math
import random

import cv2
import numpy as np


# ------------------ Rotation ------------------
def _calculate_scaling(angle_deg, image_height, image_width):
    """
    Calculates the scaling for an image with shape
    (image_height, image_width, [depth])
    so that the rotated image has no borders.

    :param angle_deg: The angle of the image to rotate in degrees
    :param image_height: The height of the original image
    :param image_width: The width of the original
    :return: The float value to use for scaling the image after rotating,
             so that no borders are visible in the rotated image
    :rtype: float
    """
    angle_rad = math.radians(abs(angle_deg))

    half_height = image_height / 2.0
    half_width = image_width / 2.0

    beta = math.atan(half_width / half_height)

    gamma = (beta - angle_rad)

    l = half_height / math.cos(gamma)

    l_l = math.sqrt(math.pow(half_height, 2) + math.pow(half_width, 2))

    scaling = l_l / l

    return scaling


def augment_rotation(image, actions, intensity):
    """
    Rotates the input image between -intensity and intensity degrees.
    The image size of the output image is equal to the size of the input image.

    Source:
    https://stackoverflow.com/questions/9041681/opencv-python-rotate-image-by-
    x-degrees-around-specific-point

    :param image: The snapshot to rotate
    :type image: image (uncompressed)
    :param actions: The actions to augment
    :type actions: dict
    :param intensity: Maximal rotation angle in degrees
    :type intensity: float

    :return: A tuple of (image, actions). The image is a rotated image with
             the same size as the input image. The actions are the input
             actions unchanged.
    :rtype: tuple[image, dict]
    """

    # create a angle between -intensity and intensity
    angle = (random.random() - 0.5) * 2.0 * intensity

    scaling = _calculate_scaling(angle, image.shape[0], image.shape[1])

    image_center = tuple(np.array(image.shape[1::-1]) / 2.0)
    rot_mat = cv2.getRotationMatrix2D(image_center, angle, scaling)
    result = cv2.warpAffine(image, rot_mat, image.shape[1::-1], flags=cv2.INTER_LINEAR)

    return result, actions


# ------------------ Brightness ------------------
def augment_brightness(image, actions, intensity):
    """
    Changes the snapshot.image brightness by a random number between
    -intensity and intensity.

    Source:
    https://stackoverflow.com/questions/32609098/how-to-fast-change-image-
    brightness-with-python-opencv

    :param image: The image to augment
    :type image: image (uncompressed)
    :param actions: The actions to augment
    :type actions: dict
    :param intensity: Defines how much the image should be
    :type intensity: int

    :return: A tuple of (image, actions). The image is darker or brighter.
             The actions are the input actions unchanged.
    :rtype: tuple[image, dict]
    """

    # create a value from -intensity to intensity
    value = int(random.randrange(intensity * 2 + 1) - intensity)

    hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    h, s, v = cv2.split(hsv)

    if value > 0:
        max_lim = 255 - value
        v[v > max_lim] = 255
        v[v <= max_lim] += value
    elif value < 0:
        min_lim = -value
        v[v < min_lim] = 0
        v[v >= min_lim] -= abs(value)

    final_hsv = cv2.merge((h, s, v))
    result = cv2.cvtColor(final_hsv, cv2.COLOR_HSV2BGR)

    return result, actions

# ------------------ Flip ------------------

# TODO Update so it works with multiple actions in Twist format

# def augment_flip(image, actions):
#     """
#     Flips the image of the snapshot and inverts the steering value of the
#     snapshot.
#
#     :param image: The image to augment
#     :type image: image (uncompressed)
#     :param actions: The actions to augment
#     :type actions: dict
#
#     :return: A tuple of (image, actions). The image is flipped vertically.
#              The actions are the input actions with inverted steer value.
#     :rtype: tuple[image, dict]
#     """
#
#     result = cv2.flip(image, 1)
#
#     if "steer" not in actions:
#         raise KeyError("Key 'steer' not found in actions")
#     actions["steer"] = -actions["steer"]
#     return result, actions
