# Augmenter

## Config File

The config file should look like this:

```yaml
# extracted_sources: "../training_data/extracted/train/"
bag_sources: ["/path/to/dir"]
destination: "/path/to/dir"
camera_topics: ["/zed/left/image_raw_color/compressed"]
action_topics: ["/nr/engine/input/actions"]
frame_skip_bags: 20
frame_skip_extracted: 20
verbose: 1
# snapshot_batch_size: 1024
rosbag_batch_size: 5

compositions:
  - name: "{source_name}_augmented1"
    count: 2
    rotation:
        intensity: 4.5
    brightness:
        intensity: 30
    blotches:
        number: 5
        min_width: 40
        max_width: 70
        min_height: 40
        max_height: 70
    lines:
        number: 10
```
