# Train

## Config File

This file could look like this:

```yaml
train_extracted_sources: /path/to/train/files/dir/ # path to training files
evaluation_extracted_sources: /path/to/eval/files/dir/ # path to evaluation files
model_dir: /path/to/models/dir/ # path to save the models
preload_data_to_ram: True # True or False; to preload the data to RAM
verbose: 1 # 0 or 1; sets verbosity of training

# Backend parameter for the used backend and learning type
backend_config:
    backend: keras # currently only keras can be used
    learning_type: supervised_learning # in the supervised learning project
                                       # supervised_learning should be used

runs: # more than one run can be given
  - num_epochs: 5 # number of training epochs
    model_name: example # name of the model; also save file name
    mode: overwrite # possible modes are: create, load, overwrite or smart
                    # create: creates a new model
                    # load: loads a existing model
                    # overwrite: creates a new model and overwrites the existing one
                    # smart: loads, if existing, the model and continues the training
    architecture: obstacle_avoidance_lecunn keras_backend_sl.py
    dual_view: True # False or True; to use two input images or just one
    batch_size: 32  # Size of the batches
    shuffle: False  # False or True; to shuffle the data
    image:
        subtract_mean: "../fixtures/featurewise_traindata_mean.npy" # images mean which gets subtracted from the images
        normalize: "../fixtures/featurewise_traindata_std_dev.npy"  # images std dev for the normalization
        resize:
            width: 90  # width of the model input image
            height: 60 # height of the model input image
            depth: 3   # depth of the model input image
            relative: False # if the resizing should be relative or absolute
        flip: null # flipping option, see description below
        crop: null # cropping option, see description below 
        opencv_color_space_conv_func: null # 7 for COLOR_BGR2GRAY
                                           # cv::ColorConversionCodes 
                                           # https://bit.ly/2NRuMZR
                                           
        decode_image: True # defines if the images must be decoded in the beginning

```
<!--

You can specify a command line argument for every entry in this configfile.
Commandline arguments override the configfile values.

### Train Sources

#### Train Extracted Sources

You can specify a main directory of extracted data created by [nrai-convert](cli/convert.py).
The data in this directory will be used to train the model.
A main-directory should have the following structure:

```
main_directory/
+-- res1/
   +-- res1.yaml
   +-- camera_topic_1/
       +-- res1_<timestamp1>.jpg
       +-- res1_<timestamp2>.jpg
   +-- camera_topic_2/
       +-- res1_<timestamp1>.jpg
       +-- res1_<timestamp2>.jpg

+-- res2/
    +-- res2.yaml
    +-- camera_topic_1/
        +-- res2_<timestamp1>.jpg
        +-- res2_<timestamp2>.jpg
    +-- [...]
```

### Evaluation Sources

#### Evaluation Extracted Sources

With `--evaluation_extracted_sources` you can specify a main directory of
extracted data created by [nrai-convert](cli/convert.py)
(In the same way as `--train_extracted_sources`).
The data in this directory will be used to evaluate the model.

-->

#### Model Dir

You can specify a directory where the data of the neural network will be stored.

#### Model Name

You can specify the name of the model.

#### Backend Config

Defines which backend (currently only "keras") and learning type (here in the supervised 
learning project mostly "supervised_learning") should be used.

#### Architecture

If a new model is created you can specify the architecture of this model.

Architectures are found in [models/keras_supervised_learning_backend.py](models/keras_backend_sl.py)

The following architectures are available:
- htw\_modified\_nvidia
- nvidia
- lillicrap
- small_net

This option will be ignored, if the model gets loaded.

#### Dual View

If the architecture uses two input images, you can set the dual view flag
so the training data gets processed correctly

#### Mode

You can set one of the following modes:

- `create` # creates a new model
- `load` # loads a existing model
- `overwrite` # creates a new model and overwrites the existing one
- `smart` # loads, if existing, the model and continues the training

##### Create

Tries to create a new model. If a model with the same name already exists it fails.

##### Load

Tries to load a existing model. If the model doesn't exists it fails.

##### Overwrite

Creates a new model. If a model with the same name already exists then it is overwritten.

##### Smart

If the model already exists it loads it, otherwise it creates a new model with the specified name.

#### Number of Epochs

Defines how many epochs to train.

#### Batch Size

Defines the used Batch Size.

#### Image Processing

To preprocess the images for the model you can provide some parameters to train.
Be aware that under certain conditions it is
necessary that the input images from the sources have all the same size.

The argument is a dictionary in inline yaml format:

##### Subtract Mean
Lets you subtract a precomputed mean from all images. The mean can be a single float value or a featurewise array of shape (1, 1, 3), which means you can set a npy file.

```yaml
subtract_mean: "/path/to/file/array.npy"
```

##### Normalize

Lets you normalize all images by dividing through a normalization value. The value can be a single float or a featurewise array of shape (1, 1, 3), which means you can set a npy file.

```yaml
normalize: "/path/to/file/array.npy"
```

##### Resize

Defines how the images are resized:

```yaml
resize:
    width:  20
    height: 30
    depth:   3
    relative: False
```

means: resize all input images to (20x30x3) pixels.

```yaml
resize: 
    width: 0.5, 
    height: 0.5,
    relative: True
```

means: resize all input images to 50% of the original size.

Be aware that when you specify `relative: True` all input images must have the same size.

##### Flip (shouldn't currently used)

Available options to flip an image.

```yaml
flip:  null # do not flip
flip:  0    # vertical flip
flip:  1    # horizontal flip
flip: -1    # horizontal_and_vertical flip
```

##### Crop

Defines how images are cropped:

```yaml
crop: null
```

means do not crop.

```yaml
crop: 
    top: 4
    bottom: 3
    left: 2
    right: 1
    relative: False
```

means crop 4 pixels on the top edge, 3 bottom, 2 left and right 1 pixel.
  
```yaml
crop:
    top: 0.2
    bottom: 0.2
    left: 0.2
    right: 0.2
    relative: True
```
means crop 20% on each side.

<!--
## Example

Given the configfile "example\_configs/train.yaml":

```yaml
train_extracted_sources: ../training_data/bags/train/
evaluation_extracted_sources: ../training_data/bags/eval/
model_dir: ../trained_data/
verbose: 0

runs:
  - num_epochs: 5
    model_name: example
    mode: overwrite
    architecture: obstacle_avoidance_lecunn
    dual_view: True
    batch_size: 32
    shuffle: False
    image:
        subtract_mean: "../fixtures/featurewise_traindata_mean.npy"
        normalize: "../fixtures/featurewise_traindata_std_dev.npy"
        resize:
            width: 90
            height: 60
            depth: 3
            relative: False
        flip: null
        crop: null
        opencv_color_space_conv_func: null
        decode_image: True
```

And rosbags in "training\_data/bags/train/" and "training\_data/bags/test/" you could do:

```bash
nrai-train --configfile example_configs/train.yaml
```

This will load the rosbags and creates 2 files "model/example.hdf5" and "model/example.nrai" to save the model data.
-->
