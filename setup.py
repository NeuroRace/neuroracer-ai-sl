#!/usr/bin/env python

from codecs import open
from os.path import abspath, dirname, join
from sys import version_info

from setuptools import setup, find_packages

with open(join(abspath(dirname(__file__)), "README.md"), encoding="utf-8") as file_handle:
    long_description = file_handle.read()

# common dependencies
install_req = [
    "cython>=0.23.4",
    "flufl.enum",  # TODO specify >= version
    "h5py>=2.6.0",
    # "neuroracer-ai>=0.5.1",  # TODO activate once packages are officially
    # "neuroracer-common>=0.4.3",  # TODO activate once packages are officially
    "numpy>=1.11.1",
    "ruamel.yaml",  # TODO specify >= version
    "pydot>=1.1.0",
    "pymp-pypi>=0.4.0",
    "scikit-learn>=0.17.1",
    "scipy>=0.18.0,<=1.1.0",  # higher version break current image processing pipeline
    "tqdm"  # TODO specify >= version
]

# handle different lib versions for python2/3
if version_info.major < 3:
    install_req.append("scikit-image>=0.12.3,<0.15.0")
    install_req.append("matplotlib<3.0.0")
    install_req.append("networkx==2.2")
else:
    install_req.append("scikit-image>=0.12.3")
    install_req.append("matplotlib>=3.0.0")

# only add when ROS is available
try:
    from catkin_pkg import python_setup

    # required for rosbag extraction
    install_req.append("rospy_message_converter>=0.3.0")
    print("INFO: ROS found. Exporting rosbags is supported.")
except ImportError:
    from warnings import warn

    warn(
        "No ROS version found on system. Exporting rosbags will not be available. To enable support after installation, install ROS for your distribution and rospy_message_converter>=0.3.0 via pypi.")

extras_req = {
    "keras_tf": ["keras>=2.0.6",
                 "keras-vis>=0.4.0",
                 "tensorflow>=1.9",
                 "tensorboard>=1.9"],
    "keras_tf_gpu": ["keras>=2.0.6",
                     "keras-vis>=0.4.0",
                     "tensorflow-gpu>=1.9",
                     "tensorboard>=1.9"],
    "torch": ["torch>=1.0.1",
              "torchvision",  # TODO specify version
              "tensorboardX>=1.2.0",
              "torchsummary>=1.5.1"]
}

setup(
    name="neuroracer-ai-sl",
    version="0.5.3",
    description="AI Supervised Learning Library for the NeuroRace Project",
    long_description=long_description,
    long_description_content_type='text/markdown',
    url="https://gitlab.com/NeuroRace",
    # author="",
    # author_email="",
    classifiers=[
        "Development Status :: 1 - BETA",
        "Intended Audience 2:: Developers",
        "Topic :: Utilities",
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python :: 2.7"
    ],
    keywords="ai supervised learning ros autonomous",
    packages=find_packages(exclude=["docs, tests"]),
    install_requires=install_req,
    extras_require=extras_req,
    test_suite="tests",
    project_urls={
        "Bug Reports": "https://gitlab.com/NeuroRace/neuroracer-ai-sl/issues",
        "Source": "https://gitlab.com/NeuroRace/neuroracer-ai-sl",
    },
    entry_points={"console_scripts": [
        "nrai-augment = neuroracer_ai.cli.augment:main",
        "nrai-convert = neuroracer_ai.cli.convert:main",
        "nrai-debug = neuroracer_ai.cli.debug:main",
        "nrai-train = neuroracer_ai.cli.train:main"
    ]}
)
