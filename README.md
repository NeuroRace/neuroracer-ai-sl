# AI Supervised Learning Package
This package is used to create and train models by supervised learning which can then be used for [Neuroracer Robot](https://gitlab.com/NeuroRace/neuroracer-robot.git) or [Neuroracer Simulator](https://gitlab.com/NeuroRace/neuroracer-sim.git).

At the moment it is only available through this repository. In near future, it will be available by pip (pypi).
    
## Content
- [Requirements](#requirements)
- [Python Version](#python-version)
- [Installation](#installation)
- [Backend](#backend)
- [Usage](#usage)
  - [Jupyter Notebooks](#jupyter-notebooks)
  - [CLI tools](#cli-tools)

## Requirements
* __neuroracer_ai__  
To use this package, [__neuroracer_ai__](https://gitlab.com/NeuroRace/neuroracer-ai.git) has to be installed.

## Python Version
For extracting data from [ROS bags](http://wiki.ros.org/Bags), due to [ROS](http://wiki.ros.org/Documentation), Python 2.7 is required, but __only__ for extracting the ROS bags.  
To speed up the preprocessing, augmentation and training, Python3.x is used in those processes.  

This results in a required install in both environments, Python2.7 (ROS bag extraction) and Python3.x (working on extracted data - processing, augmentation, training) to have the full setup. It is possible to solely use Python3.x once the recorded data is extracted.  

As soon as ROS fully supports Python3.x for simulation and robot, the Python2.7 support will be dropped.

## Installation
To install the library clone the repository and use `pip install -e`. The command will install the package in editable mode. 
To install in both python environments, call pip specifically.

pip2 installation is __only__ needed for extracting data from ROS bags. You can use the pip3 installation if you have
extracted data and just want to use those.

```bash
git clone https://gitlab.com/NeuroRace/neuroracer-ai-sl.git
pip2 install --user -e neuroracer-ai-sl[backend]
pip3 install --user -e neuroracer-ai-sl[backend]
```

where `backend` can be either [Keras](https://github.com/keras-team/keras) with [Tensorflow](https://github.com/tensorflow/tensorflow) or [PyTorch](https://github.com/pytorch/pytorch). The available install options are
* `keras_tf` (Keras with Tensorflow backend - cpu only)
* `keras_tf_gpu` (Keras with Tensorflow backend - gpu support)
* `torch` (dynamically selects cpu/gpu)

So for example if you want to install the PyTorch backend for python3.x, the command would be
```
pip3 install --user -e neuroracer-ai-sl[torch]
```
Of course you can install Keras and PyTorch side by side and switch via config file any time. See [Backend](#backend) section for further information.

## Backend
Since the supervised learning package inherites from **neuroracer-ai**, the backend handling is the same. For information how-to [see here](https://gitlab.com/NeuroRace/neuroracer-ai#backend).

## Usage
The following will explain the provided functions and their usage. 

### Jupyter Notebooks
See [notebooks](notebooks) for some example implementations on how to use the api. These are for supervised learning. The basic api methods are described in [neuroracer-ai](https://gitlab.com/NeuroRace/neuroracer-ai.git). 

* [How to create an AI](notebooks/01_how_to_create_ai_sl.ipynb)
* [How to train an AI](notebooks/02_how_to_train_ai_sl.ipynb)
* [How to save an AI](notebooks/03_how_to_save_ai_sl.ipynb)


### CLI tools
Besides the api itself, the package contains helpful cli tools:
- `augment.py`

  See [the cli augment.py usage](./neuroracer_ai_sl/cli-augment.md) (not up to date) 

- `analyze.py`

  See [the cli analyze.py usage](./neuroracer_ai_sl/cli-analyze.md)


- `converter.py`

  See [the cli convert.py usage](./neuroracer_ai_sl/cli-convert.md) 
  
- `train.py`

  See [the cli train.py usage](./neuroracer_ai_sl/cli-train.md) 
